﻿namespace NaturalOptimizationAlgorithmsTSP
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabNaturalAlgorithms = new System.Windows.Forms.TabControl();
            this.tabGenetic = new System.Windows.Forms.TabPage();
            this.tabBees = new System.Windows.Forms.TabPage();
            this.tabNN = new System.Windows.Forms.TabPage();
            this.tabGreedy = new System.Windows.Forms.TabPage();
            this.tabMassPoint = new System.Windows.Forms.TabPage();
            this.tabConvexHullRings = new System.Windows.Forms.TabPage();
            this.tabGenRelatChromosome = new System.Windows.Forms.TabPage();
            this.geneticUC2 = new NaturalOptimizationAlgorithmsTSP.CustomControls.GeneticUC();
            this.beesUC1 = new NaturalOptimizationAlgorithmsTSP.CustomControls.BeesUC();
            this.nearestNeighbourUC1 = new NaturalOptimizationAlgorithmsTSP.CustomControls.NearestNeighbourUC();
            this.greedyUC1 = new NaturalOptimizationAlgorithmsTSP.CustomControls.GreedyUC();
            this.massPointUC1 = new NaturalOptimizationAlgorithmsTSP.CustomControls.MassPointUC();
            this.convexHullRingsUC1 = new NaturalOptimizationAlgorithmsTSP.CustomControls.ConvexHullRingsUC();
            this.geneticAlgorithmRelationChromosomeUC1 = new NaturalOptimizationAlgorithmsTSP.CustomControls.GeneticAlgorithmRelationChromosomeUC();
            this.tabNaturalAlgorithms.SuspendLayout();
            this.tabGenetic.SuspendLayout();
            this.tabBees.SuspendLayout();
            this.tabNN.SuspendLayout();
            this.tabGreedy.SuspendLayout();
            this.tabMassPoint.SuspendLayout();
            this.tabConvexHullRings.SuspendLayout();
            this.tabGenRelatChromosome.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabNaturalAlgorithms
            // 
            this.tabNaturalAlgorithms.Controls.Add(this.tabGenetic);
            this.tabNaturalAlgorithms.Controls.Add(this.tabBees);
            this.tabNaturalAlgorithms.Controls.Add(this.tabNN);
            this.tabNaturalAlgorithms.Controls.Add(this.tabGreedy);
            this.tabNaturalAlgorithms.Controls.Add(this.tabMassPoint);
            this.tabNaturalAlgorithms.Controls.Add(this.tabConvexHullRings);
            this.tabNaturalAlgorithms.Controls.Add(this.tabGenRelatChromosome);
            this.tabNaturalAlgorithms.Location = new System.Drawing.Point(13, 33);
            this.tabNaturalAlgorithms.Name = "tabNaturalAlgorithms";
            this.tabNaturalAlgorithms.SelectedIndex = 0;
            this.tabNaturalAlgorithms.Size = new System.Drawing.Size(1189, 579);
            this.tabNaturalAlgorithms.TabIndex = 0;
            // 
            // tabGenetic
            // 
            this.tabGenetic.Controls.Add(this.geneticUC2);
            this.tabGenetic.Location = new System.Drawing.Point(4, 25);
            this.tabGenetic.Name = "tabGenetic";
            this.tabGenetic.Padding = new System.Windows.Forms.Padding(3);
            this.tabGenetic.Size = new System.Drawing.Size(1181, 550);
            this.tabGenetic.TabIndex = 0;
            this.tabGenetic.Text = "Genetic Algorithm";
            this.tabGenetic.UseVisualStyleBackColor = true;
            // 
            // tabBees
            // 
            this.tabBees.Controls.Add(this.beesUC1);
            this.tabBees.Location = new System.Drawing.Point(4, 25);
            this.tabBees.Name = "tabBees";
            this.tabBees.Padding = new System.Windows.Forms.Padding(3);
            this.tabBees.Size = new System.Drawing.Size(1181, 550);
            this.tabBees.TabIndex = 2;
            this.tabBees.Text = "Bee Colony";
            this.tabBees.UseVisualStyleBackColor = true;
            // 
            // tabNN
            // 
            this.tabNN.Controls.Add(this.nearestNeighbourUC1);
            this.tabNN.Location = new System.Drawing.Point(4, 25);
            this.tabNN.Name = "tabNN";
            this.tabNN.Padding = new System.Windows.Forms.Padding(3);
            this.tabNN.Size = new System.Drawing.Size(1181, 550);
            this.tabNN.TabIndex = 3;
            this.tabNN.Text = "Nearest Neighbour";
            this.tabNN.UseVisualStyleBackColor = true;
            // 
            // tabGreedy
            // 
            this.tabGreedy.Controls.Add(this.greedyUC1);
            this.tabGreedy.Location = new System.Drawing.Point(4, 25);
            this.tabGreedy.Name = "tabGreedy";
            this.tabGreedy.Padding = new System.Windows.Forms.Padding(3);
            this.tabGreedy.Size = new System.Drawing.Size(1181, 550);
            this.tabGreedy.TabIndex = 4;
            this.tabGreedy.Text = "Greedy";
            this.tabGreedy.UseVisualStyleBackColor = true;
            // 
            // tabMassPoint
            // 
            this.tabMassPoint.Controls.Add(this.massPointUC1);
            this.tabMassPoint.Location = new System.Drawing.Point(4, 25);
            this.tabMassPoint.Name = "tabMassPoint";
            this.tabMassPoint.Padding = new System.Windows.Forms.Padding(3);
            this.tabMassPoint.Size = new System.Drawing.Size(1181, 550);
            this.tabMassPoint.TabIndex = 5;
            this.tabMassPoint.Text = "MassPoint";
            this.tabMassPoint.UseVisualStyleBackColor = true;
            // 
            // tabConvexHullRings
            // 
            this.tabConvexHullRings.Controls.Add(this.convexHullRingsUC1);
            this.tabConvexHullRings.Location = new System.Drawing.Point(4, 25);
            this.tabConvexHullRings.Name = "tabConvexHullRings";
            this.tabConvexHullRings.Padding = new System.Windows.Forms.Padding(3);
            this.tabConvexHullRings.Size = new System.Drawing.Size(1181, 550);
            this.tabConvexHullRings.TabIndex = 6;
            this.tabConvexHullRings.Text = "ConvexHullRings";
            this.tabConvexHullRings.UseVisualStyleBackColor = true;
            // 
            // tabGenRelatChromosome
            // 
            this.tabGenRelatChromosome.Controls.Add(this.geneticAlgorithmRelationChromosomeUC1);
            this.tabGenRelatChromosome.Location = new System.Drawing.Point(4, 25);
            this.tabGenRelatChromosome.Name = "tabGenRelatChromosome";
            this.tabGenRelatChromosome.Padding = new System.Windows.Forms.Padding(3);
            this.tabGenRelatChromosome.Size = new System.Drawing.Size(1181, 550);
            this.tabGenRelatChromosome.TabIndex = 7;
            this.tabGenRelatChromosome.Text = "GeneticAlgorithmRelationChromosome";
            this.tabGenRelatChromosome.UseVisualStyleBackColor = true;
            // 
            // geneticUC2
            // 
            this.geneticUC2.AutoSize = true;
            this.geneticUC2.CrossoverRate = 0.2D;
            this.geneticUC2.ElitismCount = 0;
            this.geneticUC2.GreedyRate = 0.1D;
            this.geneticUC2.Location = new System.Drawing.Point(3, 6);
            this.geneticUC2.MutationRate = 0.05D;
            this.geneticUC2.Name = "geneticUC2";
            this.geneticUC2.NnaRate = 0.1D;
            this.geneticUC2.PopulationSize = 100;
            this.geneticUC2.Size = new System.Drawing.Size(1162, 510);
            this.geneticUC2.TabIndex = 0;
            this.geneticUC2.TurnamentSize = 80;
            // 
            // beesUC1
            // 
            this.beesUC1.Location = new System.Drawing.Point(7, 4);
            this.beesUC1.Name = "beesUC1";
            this.beesUC1.Size = new System.Drawing.Size(1162, 510);
            this.beesUC1.TabIndex = 0;
            // 
            // nearestNeighbourUC1
            // 
            this.nearestNeighbourUC1.Location = new System.Drawing.Point(6, 6);
            this.nearestNeighbourUC1.Name = "nearestNeighbourUC1";
            this.nearestNeighbourUC1.Size = new System.Drawing.Size(1162, 510);
            this.nearestNeighbourUC1.TabIndex = 0;
            // 
            // greedyUC1
            // 
            this.greedyUC1.Location = new System.Drawing.Point(4, 7);
            this.greedyUC1.Name = "greedyUC1";
            this.greedyUC1.Size = new System.Drawing.Size(1162, 510);
            this.greedyUC1.TabIndex = 0;
            // 
            // massPointUC1
            // 
            this.massPointUC1.Location = new System.Drawing.Point(4, 7);
            this.massPointUC1.Name = "massPointUC1";
            this.massPointUC1.Size = new System.Drawing.Size(1162, 510);
            this.massPointUC1.TabIndex = 0;
            // 
            // convexHullRingsUC1
            // 
            this.convexHullRingsUC1.Location = new System.Drawing.Point(9, 18);
            this.convexHullRingsUC1.Name = "convexHullRingsUC1";
            this.convexHullRingsUC1.Size = new System.Drawing.Size(1162, 510);
            this.convexHullRingsUC1.TabIndex = 0;
            // 
            // geneticAlgorithmRelationChromosomeUC1
            // 
            this.geneticAlgorithmRelationChromosomeUC1.CrossoverRate = 0.75D;
            this.geneticAlgorithmRelationChromosomeUC1.ElitismCount = 0;
            this.geneticAlgorithmRelationChromosomeUC1.GreedyRate = 0.1D;
            this.geneticAlgorithmRelationChromosomeUC1.Location = new System.Drawing.Point(7, 7);
            this.geneticAlgorithmRelationChromosomeUC1.MutationRate = 0.25D;
            this.geneticAlgorithmRelationChromosomeUC1.Name = "geneticAlgorithmRelationChromosomeUC1";
            this.geneticAlgorithmRelationChromosomeUC1.NnaRate = 0.1D;
            this.geneticAlgorithmRelationChromosomeUC1.PopulationSize = 100;
            this.geneticAlgorithmRelationChromosomeUC1.Size = new System.Drawing.Size(1163, 510);
            this.geneticAlgorithmRelationChromosomeUC1.TabIndex = 0;
            this.geneticAlgorithmRelationChromosomeUC1.TurnamentSize = 2;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1215, 623);
            this.Controls.Add(this.tabNaturalAlgorithms);
            this.Name = "Form1";
            this.Text = "Form1";
            this.tabNaturalAlgorithms.ResumeLayout(false);
            this.tabGenetic.ResumeLayout(false);
            this.tabGenetic.PerformLayout();
            this.tabBees.ResumeLayout(false);
            this.tabNN.ResumeLayout(false);
            this.tabGreedy.ResumeLayout(false);
            this.tabMassPoint.ResumeLayout(false);
            this.tabConvexHullRings.ResumeLayout(false);
            this.tabGenRelatChromosome.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabNaturalAlgorithms;
        private System.Windows.Forms.TabPage tabGenetic;
        private System.Windows.Forms.TabPage tabBees;
        private CustomControls.GeneticUC geneticUC2;
        private System.Windows.Forms.TabPage tabNN;
        private CustomControls.NearestNeighbourUC nearestNeighbourUC1;
        private System.Windows.Forms.TabPage tabGreedy;
        private CustomControls.GreedyUC greedyUC1;
        private System.Windows.Forms.TabPage tabMassPoint;
        private CustomControls.MassPointUC massPointUC1;
        private CustomControls.BeesUC beesUC1;
        private System.Windows.Forms.TabPage tabConvexHullRings;
        private CustomControls.ConvexHullRingsUC convexHullRingsUC1;
        private System.Windows.Forms.TabPage tabGenRelatChromosome;
        private CustomControls.GeneticAlgorithmRelationChromosomeUC geneticAlgorithmRelationChromosomeUC1;
    }
}

