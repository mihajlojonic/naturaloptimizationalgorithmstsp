﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithmReletionChromosome
{
    class Individual
    {
        private GeneAsRelation[] chromosome;
        private double fitness = -1;

        internal GeneAsRelation[] Chromosome { get => chromosome; set => chromosome = value; }
        public double Fitness { get => fitness; set => fitness = value; }

        public Individual(GeneAsRelation[] chromosome)
        {
            this.Chromosome = chromosome;
        }

        public Individual(int chromosomeLength)
        {
            GeneAsRelation[] individual;
            individual = new GeneAsRelation[chromosomeLength];

            for (int gene = 0; gene < chromosomeLength; gene++)
                individual[gene] = new GeneAsRelation();

            this.Chromosome = individual;
        }

        public Individual(int chromosomeLength, bool shuffle)
        {
            GeneAsRelation[] individual;
            individual = new GeneAsRelation[chromosomeLength];

            for (int gene = 0; gene < chromosomeLength; gene++)
                individual[gene] = new GeneAsRelation();

            Random rnd = new Random();
            for(int i = 2; i < chromosomeLength - 2; i++)
            {
                int r = rnd.Next(i, chromosomeLength - 2);
                GeneAsRelation tmp = individual[r];
                individual[r] = individual[i];
                individual[i] = tmp;
            }

            this.Chromosome = individual;
        }

        public GeneAsRelation[] initChromosome(City[] cities)
        {
            for(int i = 0; i < Chromosome.Length - 1; i++)
            {
                Chromosome[i] = new GeneAsRelation(cities[i], cities[i + 1]);
            }

            Chromosome[Chromosome.Length - 1] = new GeneAsRelation(cities[cities.Length - 1], cities[0]);

            return this.Chromosome;
        }

        public double getRouteDistance()
        {
            double distanceReturn = 0;
            foreach (GeneAsRelation gene in Chromosome)
                distanceReturn += gene.Distance;

            if (distanceReturn == 0)
                return 1;
            else
                return distanceReturn;
        }

        public bool constainsGene(GeneAsRelation gene)
        {
            for (int i = 0; i < this.Chromosome.Length; i++)
                if (this.Chromosome[i].Equals(gene))
                    return true;
            return false;
        }

        public int getChromosomeLength()
        {
            return this.Chromosome.Length;
        }

        public void setGene(int offset, GeneAsRelation gene)
        {
            this.Chromosome[offset] = gene;
        }

        public GeneAsRelation getGene(int offset)
        {
            return this.Chromosome[offset];
        }
    }
}
