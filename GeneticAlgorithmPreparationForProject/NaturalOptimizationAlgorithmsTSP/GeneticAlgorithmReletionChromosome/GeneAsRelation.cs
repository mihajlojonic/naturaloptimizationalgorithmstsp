﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithmReletionChromosome
{
    class GeneAsRelation
    {
        City city1;
        City city2;
        double distance;

        public double Distance { get => distance; }
        public City City1 { get => city1; set => city1 = value; }
        public City City2 { get => city2; set => city2 = value; }

        public GeneAsRelation(City c1, City c2)
        {
            this.City1 = c1;
            this.City2 = c2;

            this.distance = this.City1.distanceFrom(this.City2);
        }

        public GeneAsRelation()
        {
            City1 = null;
            City2 = null;
            distance = -1;
        }

        public GeneAsRelation(GeneAsRelation oldGene)
        {
            this.city1 = oldGene.city1;
            this.city2 = oldGene.city2;
            this.distance = oldGene.distance;
        }

        public override bool Equals(object obj)
        {
            return (this.City1.X == (obj as GeneAsRelation).City1.X) &&
                   (this.City1.Y == (obj as GeneAsRelation).City1.Y) &&
                   (this.City2.X == (obj as GeneAsRelation).City2.X) &&
                   (this.City2.Y == (obj as GeneAsRelation).City2.Y);
        }
    }
}
