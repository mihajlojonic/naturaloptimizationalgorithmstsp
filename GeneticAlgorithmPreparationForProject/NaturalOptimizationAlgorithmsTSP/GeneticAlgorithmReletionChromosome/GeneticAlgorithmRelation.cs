﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithmReletionChromosome
{
    class GeneticAlgorithmRelation
    {
        private int generationCount = 1;
        public static int maxGenerations = 3000;
        public static double optimalFitness = 0.99;

        private int populationSize;
        private double mutationRate;
        private double crossoverRate;
        private int elitismCount;
        protected int turnamentSize;

        public GeneticAlgorithmRelation(int populationSize, double mutationRate, double crossoverRate, int elitismCount, int turnamentSize)
        {
            this.populationSize = populationSize;
            this.mutationRate = mutationRate;
            this.crossoverRate = crossoverRate;
            this.elitismCount = elitismCount;
            this.turnamentSize = turnamentSize;
        }

        public double calcFitness(Individual individual)
        {
            individual.Fitness = 1 / individual.getRouteDistance();
            return individual.Fitness;
        }

        public void evalPopulation(Population population)
        {
            double populationFitness = 0;

            foreach (Individual individual in population.PopulationArray)
                populationFitness += this.calcFitness(individual);

            double avgFitness = populationFitness / population.size();
            population.PopulationFitness = avgFitness;
        }

        public bool isTerminateConditionMet(int generationCount, int maxGenerations)
        {
            return (generationCount > maxGenerations);
        }

        public Population initPopulation(int chromosomeLength, int countOfSpecialInitialize, City[] cities)
        {
            Population population = new Population(this.populationSize, chromosomeLength, countOfSpecialInitialize);

            foreach(Individual individual in population.PopulationArray)
            {
                individual.Chromosome = individual.initChromosome(cities);
                individual.Chromosome = this.shuffleGenesInitialisation(individual.Chromosome);
            }

            return population;
        }

        public void run()
        {
            
            int numCities = 100;
            City[] cities = new City[numCities];

            Random rnd = new Random();
            for (int cityIndex = 0; cityIndex < numCities; cityIndex++)
            {
                int xPos = rnd.Next(100);
                int yPos = rnd.Next(100);
                cities[cityIndex] = new City(xPos, yPos);
            }

            Population population = this.initPopulation(cities.Length, 0, cities);
            this.evalPopulation(population);

            while (this.isTerminateConditionMet(generationCount, maxGenerations) == false)
            {
                Individual fittest = population.getFittest(0);
                Console.WriteLine("G: " + generationCount + " Best distance: " + fittest.getRouteDistance());

                population = this.crossoverPopulation(population);
                population = this.mutatePopulation(population);

                this.evalPopulation(population);

                generationCount++;
            }
            Console.WriteLine("Stopped after: " + maxGenerations + " generations.");
            Individual fittestEnd = population.getFittest(0);
            Console.WriteLine("Best distance: " + fittestEnd.getRouteDistance());
            
        }

        public Individual selectParent(Population population)
        {
            //elite selection
            Population tournament = new Population(this.turnamentSize);
            population.shuffle();
            for (int i = 0; i < this.turnamentSize; i++)
            {
                Individual tournamentIndividual = population.getIndividual(i);
                tournament.setIndividual(i, tournamentIndividual);
            }

            return tournament.getFittest(0);
        }

        public Population crossoverPopulation(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for(int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual parent1 = population.getFittest(populationIndex);

                if (this.crossoverRate > rnd.NextDouble() && populationIndex >= elitismCount)
                {
                    Individual parent2 = this.selectParent(population);
                    GeneAsRelation[] offspringChromosome = new GeneAsRelation[parent1.getChromosomeLength()];
                    for (int i = 0; i < offspringChromosome.Length; i++)
                        offspringChromosome[i] = new GeneAsRelation();
                    Individual offspreing = new Individual(offspringChromosome);

                    //alternately adding genes, first from first second from second parent and so one
                    offspreing.setGene(0, parent1.getGene(0));
                    City secondCity = parent1.getGene(0).City2;
                    for (int i = 1; i < offspringChromosome.Length; i++)
                    {
                        if (i % 2 == 0)//add from first
                        {
                            GeneAsRelation firstParentGene = new GeneAsRelation(parent1.Chromosome.Where(c => c.City1 == secondCity).First());
                            offspreing.setGene(i, firstParentGene);

                            secondCity = firstParentGene.City2;
                        }
                        else//add from second
                        {
                            GeneAsRelation secondParentGene = new GeneAsRelation(parent2.Chromosome.Where(c => c.City1 == secondCity).First());
                            offspreing.setGene(i, secondParentGene);

                            secondCity = secondParentGene.City2;
                        }
                    }

                    newPopulation.setIndividual(populationIndex, offspreing);
                }
                else
                    newPopulation.setIndividual(populationIndex, parent1);
            }


            return newPopulation;
        }

        public Population mutatePopulation(Population population)
        {
            
            Population newPopulation = new Population(this.populationSize);
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual individual = population.getFittest(populationIndex);

                if (populationIndex >= this.elitismCount)
                {
                    for (int geneIndex = 0; geneIndex < individual.getChromosomeLength(); geneIndex++)
                    {
                        if (this.mutationRate > rnd.NextDouble())
                        {
                            int newGenPos = (int)(rnd.NextDouble() * individual.getChromosomeLength());
                            GeneAsRelation gene1 = individual.getGene(newGenPos);
                            GeneAsRelation gene2 = individual.getGene(geneIndex);

                            City city10 = gene1.City1;
                            City city21 = gene2.City2;

                            gene1.City1 = city21;
                            gene2.City2 = city10;
                             
                            individual.setGene(newGenPos, gene1);
                            individual.setGene(geneIndex, gene2);
                        }
                    }
                }
                newPopulation.setIndividual(populationIndex, individual);
            }
            return newPopulation;
        }

        public GeneAsRelation[] shuffleGenesInitialisation(GeneAsRelation[] chromosome)
        {
            Random rnd = new Random();
            for (int geneIndex = 0; geneIndex < chromosome.Length; geneIndex++)
            {
                if (this.mutationRate > rnd.NextDouble())
                {
                    int newGenPos = (int)(rnd.NextDouble() * chromosome.Length);
                    GeneAsRelation gene1 = chromosome[newGenPos];
                    GeneAsRelation gene2 = chromosome[geneIndex];

                    City city10 = gene1.City1;
                    City city21 = gene2.City2;

                    gene1.City1 = city21;
                    gene2.City2 = city10;

                    chromosome[newGenPos] = gene1;
                    chromosome[newGenPos] = gene2;
                }
            }

            return chromosome;
        }
    }

}
