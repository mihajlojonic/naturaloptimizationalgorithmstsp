﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.BeeColonyAlgorithm
{
    class Hive
    {
        Random rnd = new Random();
        public City[] cities;

        public int totalNumberBees;
        public int numberInactive;
        public int numberActive;
        public int numberScout;

        public int maxNumberCycles;

        public int maxNumberVisits;
        public double probPersuasion = 0.95;
        public double probMistake = 0.01;

        public int numberOfSpecialInitialize = 0;

        public Bee[] bees;
        public Bee bestBee;
        public int[] indexesOfInactiveBees;

        public Hive(int totalNumberBees, int numberInactive, int numberActive, int numberScout,
                    int maxNumberVisits, int maxNumberCycles, double probPersuasion, double probMistake, City[] cities, int countOfSpecialInitialize)
        {
            this.totalNumberBees = totalNumberBees;
            this.numberInactive = numberInactive;
            this.numberActive = numberActive;
            this.numberScout = numberScout;
            this.maxNumberVisits = maxNumberVisits;
            this.maxNumberCycles = maxNumberCycles;

            this.cities = cities;

            this.numberOfSpecialInitialize = countOfSpecialInitialize;

            this.bees = new Bee[totalNumberBees];

            InitializeHive();
        }

        private void InitializeHive()
        {
            int[] bestSolution = GenerateRandomSolution();
            bestBee = new Bee(GenerateRandomSolution());
            bestBee.fitness = calculateFitness(bestBee);

            this.indexesOfInactiveBees = new int[numberInactive];

            for (int i = 0; i < totalNumberBees; ++i)
            {
                BeeStatus status;
                if (i < numberInactive)
                {
                    status = BeeStatus.inactive;
                    indexesOfInactiveBees[i] = i;
                }
                else if (i < numberInactive + numberScout)
                    status = BeeStatus.scout;
                else
                    status = BeeStatus.active;

                int[] randomSolution = new int[cities.Length];

                if(i < numberOfSpecialInitialize)
                    randomSolution = GenerateSpecialSolution();
                else
                    randomSolution = GenerateRandomSolution();

                int numberOfVisits = 0;

                bees[i] = new Bee(status, randomSolution, 0, numberOfVisits);
                bees[i].fitness = calculateFitness(bees[i]);

                if (bees[i].fitness > bestBee.fitness)
                {
                    Array.Copy(bees[i].solutionRoute, bestBee.solutionRoute, bees[i].solutionRoute.Length);
                    bestBee.fitness = bees[i].fitness;
                    bestBee.numberOfVisits = bees[i].numberOfVisits;
                }
            }
        }

        private int[] GenerateSpecialSolution()
        {
            int[] result = new int[this.cities.Length];

            for (int i = 0; i < result.Length; i++)
                result[i] = i;

            return result;
        }

        public int[] GenerateRandomSolution()
        {
            int[] result = new int[this.cities.Length];
            
            for (int i = 0; i < result.Length; i++)
                result[i] = i;

            for (int i = 0; i < result.Length; i++)
            {
                int r = rnd.Next(i, result.Length);
                int temp = result[r];
                result[r] = result[i];
                result[i] = temp;
            }
            return result;
        }

        public int[] GenerateNeighbourSolution(int[] solution)
        {
            int[] result = new int[solution.Length];
            Array.Copy(solution, result, solution.Length);

            int ranIndex = rnd.Next(0, result.Length);
            int adjIndex;
            if (ranIndex == result.Length - 1)
                adjIndex = 0;
            else
                adjIndex = ranIndex + 1;

            int tmp = result[ranIndex];
            result[ranIndex] = result[adjIndex];
            result[adjIndex] = tmp;

            return result;
        }

        public double calculateFitness(Bee bee)
        {
            double answer = 0.0;
            Route route = new Route(bee, cities);
            answer = 1 / route.getDistance();
            return answer;
        }

        public void ProcessActiveBee(int i)
        {
            int[] neighborSolution = GenerateNeighbourSolution(bees[i].solutionRoute);
            Bee neighborBee = new Bee(neighborSolution);
            double neighborFitness = calculateFitness(neighborBee);
            neighborBee.fitness = neighborFitness;
            double prob = rnd.NextDouble(); 
            bool memoryWasUpdated = false; 
            bool numberOfVisitsOverLimit = false; 

            if (neighborFitness > bees[i].fitness) 
            {
                if (prob < probMistake) 
                {
                    ++bees[i].numberOfVisits; 
                    if (bees[i].numberOfVisits > maxNumberVisits) numberOfVisitsOverLimit = true;
                }
                else 
                {
                    Array.Copy(neighborBee.solutionRoute, bees[i].solutionRoute, neighborBee.solutionRoute.Length); 
                    bees[i].fitness = neighborBee.fitness; 
                    bees[i].numberOfVisits = 0; 
                    memoryWasUpdated = true; 
                }
            }
            else 
            {
                if (prob < probMistake) 
                {
                    Array.Copy(neighborBee.solutionRoute, bees[i].solutionRoute, neighborBee.solutionRoute.Length);
                    bees[i].fitness = neighborBee.fitness;
                    bees[i].numberOfVisits = 0; 
                    memoryWasUpdated = true; 
                }
                else 
                {
                    ++bees[i].numberOfVisits;
                    if (bees[i].numberOfVisits > maxNumberVisits) numberOfVisitsOverLimit = true;
                }
            }
            
            ProcessInactiveBee(i);
            
            if (numberOfVisitsOverLimit == true)
            {
                bees[i].status = BeeStatus.inactive; 
                bees[i].numberOfVisits = 0; 
                int x = rnd.Next(numberInactive); 
                bees[indexesOfInactiveBees[x]].status = BeeStatus.active; 
                indexesOfInactiveBees[x] = i; 
            }
            else if (memoryWasUpdated == true)
            {
                if (bees[i].fitness > this.bestBee.fitness) 
                {
                    Array.Copy(bees[i].solutionRoute, this.bestBee.solutionRoute, bees[i].solutionRoute.Length); 
                    this.bestBee.fitness = bees[i].fitness; 
                }
                DoWaggleDance(i);
            }
            else 
            {
                return;
            }
        }

        public void ProcessScoutBee(int i)
        {
            int[] scoutSolution = GenerateRandomSolution();
            Bee scoutBee = new Bee(scoutSolution);
            scoutBee.fitness = calculateFitness(scoutBee);
            
            if (scoutBee.fitness > bees[i].fitness)
            {
                Array.Copy(scoutBee.solutionRoute, bees[i].solutionRoute, scoutBee.solutionRoute.Length);
                bees[i].fitness = scoutBee.fitness;
                bees[i].numberOfVisits = scoutBee.numberOfVisits;

                if (bees[i].fitness > bestBee.fitness)
                {
                    Array.Copy(bees[i].solutionRoute, bestBee.solutionRoute, bees[i].solutionRoute.Length); 
                    this.bestBee.fitness = bees[i].fitness;
                    this.bestBee.numberOfVisits = bees[i].numberOfVisits;
                } 
                DoWaggleDance(i); 
            }
            
            ProcessInactiveBee(i);
            
        }

        public void ProcessInactiveBee(int i)
        {
            Random rnd = new Random();
            int jump = rnd.Next(0,bees[i].solutionRoute.Length / 3);

            for(int index = 0; index < bees[i].solutionRoute.Length; index++)
            {
                int[] tempSolution = new int[bees[i].solutionRoute.Length];
                Array.Copy(bees[i].solutionRoute, tempSolution, tempSolution.Length);

                Bee tempBee = new Bee(tempSolution);
                tempBee.fitness = calculateFitness(tempBee);
                tempBee.numberOfVisits = bees[i].numberOfVisits;

                int temp = tempBee.solutionRoute[(i * jump) % bees[i].solutionRoute.Length];
                tempBee.solutionRoute[(i * jump) % bees[i].solutionRoute.Length] = tempBee.solutionRoute[index];
                tempBee.solutionRoute[index] = temp;

                tempBee.fitness = calculateFitness(tempBee);

                if (bees[i].fitness < tempBee.fitness)
                    Array.Copy(tempBee.solutionRoute, bees[i].solutionRoute, tempBee.solutionRoute.Length);
                bees[i].fitness = calculateFitness(bees[i]);
            }

        }

        private void DoWaggleDance(int i)
        {
            for (int ii = 0; ii < numberInactive; ++ii) 
            {
                int b = indexesOfInactiveBees[ii]; 
                
                if (bees[i].fitness > bees[b].fitness) 
                {
                    double p = rnd.NextDouble(); 
                    if (this.probPersuasion > p) 
                    {
                        Array.Copy(bees[i].solutionRoute, bees[b].solutionRoute, bees[i].solutionRoute.Length);
                        bees[b].fitness = bees[i].fitness;
                        bees[b].numberOfVisits = bees[i].numberOfVisits;
                    } 
                } 
            } 
        }
    }
}
