﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.BeeColonyAlgorithm
{
    public enum BeeStatus
    {
        inactive,
        active,
        scout
    }

    public class Bee
    {
        public BeeStatus status { get; set; }
        public int[] solutionRoute; 
        public double fitness; 
        public int numberOfVisits;

        public Bee(BeeStatus status, int[] solution, double fitness, int numberOfVisits)
        {
            this.status = status;
            this.solutionRoute = new int[solution.Length];
            Array.Copy(solution, this.solutionRoute, solution.Length);
            this.fitness = fitness;
            this.numberOfVisits = numberOfVisits;
        }

        public Bee(int[] solution)
        {
            this.solutionRoute = new int[solution.Length];
            Array.Copy(solution, this.solutionRoute, solution.Length);
        }

        public override string ToString()
        {
            string s = "";
            s += "Status = " + this.status + "\n";
            s += " Memory = " + "\n";
            for (int i = 0; i < this.solutionRoute.Length - 1; i++)
                s += this.solutionRoute[i] + "->";
            s += this.solutionRoute[this.solutionRoute.Length - 1] + "\n";
            s += " Quality = " + this.fitness.ToString("F4");
            s += " Number visits = " + this.numberOfVisits;

            return s;
        }
    }
}
