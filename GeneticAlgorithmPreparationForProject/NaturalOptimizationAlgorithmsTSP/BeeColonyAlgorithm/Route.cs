﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.BeeColonyAlgorithm
{
    class Route
    {
        public City[] route;
        private double distance = 0;

        public Route(Bee bee, City[] cities)
        {
            int[] solutionRoute = bee.solutionRoute;
            this.route = new City[cities.Length];
            for (int cityIndex = 0; cityIndex < solutionRoute.Length; cityIndex++)
                this.route[cityIndex] = cities[solutionRoute[cityIndex]];
        }

        public double getDistance()
        {
            if (this.distance > 0)
                return this.distance;

            double totalDistance = 0;
            for (int cityIndex = 0; cityIndex + 1 < this.route.Length; cityIndex++)
                totalDistance += this.route[cityIndex].distanceFrom(this.route[cityIndex + 1]);

            totalDistance += this.route[this.route.Length - 1].distanceFrom(this.route[0]);
            this.distance = totalDistance;

            return totalDistance;
        }
    }
}
