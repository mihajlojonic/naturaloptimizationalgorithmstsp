﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm
{
    public class Individual
    {
        private int[] chromosome;
        private double fitness = -1;

        public int[] Chromosome { get => chromosome; set => chromosome = value; }
        public double Fitness { get => fitness; set => fitness = value; }

        public Individual(int[] chromosome)
        {
            this.Chromosome = chromosome;
        }

        public Individual(int chromosomeLength)
        {
            int[] individual;
            individual = new int[chromosomeLength];

            for (int gene = 0; gene < chromosomeLength; gene++)
                individual[gene] = gene;

            this.Chromosome = individual;
        }

        public Individual(int chromosomeLength, bool shuffle)
        {
            int[] individual;
            individual = new int[chromosomeLength];

            for (int gene = 0; gene < chromosomeLength; gene++)
                individual[gene] = gene;

            Random rnd = new Random();
            for (int i = 2; i < chromosomeLength - 2; i++)
            {
                int r = rnd.Next(i, chromosomeLength - 2);
                int tmp = individual[r];
                individual[r] = individual[i];
                individual[i] = tmp;
            }
            this.Chromosome = individual;
        }

        public bool containsGene(int gene)
        {
            for (int i = 0; i < this.Chromosome.Length; i++)
                if (this.Chromosome[i] == gene)
                    return true;
            return false;
        }

        public int getChromosomeLength()
        {
            return this.Chromosome.Length;
        }

        public void setGene(int offset, int gene)
        {
            this.Chromosome[offset] = gene;
        }

        public int getGene(int offset)
        {
            return this.Chromosome[offset];
        }



        public String toString()
        {
            String output = "";
            for (int gene = 0; gene < this.Chromosome.Length; gene++)
                output += this.Chromosome[gene];
            return output;
        }
    }
}
