﻿using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Corossovers
{
    class OrderedCrossover : ICrossover
    {
        private int elitismCount;
        private double crossoverRate;
        private ISelection selection;

        public OrderedCrossover(int elitismCount, double crossoverRate, ISelection selection)
        {
            this.elitismCount = elitismCount;
            this.crossoverRate = crossoverRate;
            this.selection = selection;
        }

        public Population crossover(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual parent1 = population.getFittest(populationIndex);

                if (this.crossoverRate > rnd.NextDouble() && populationIndex >= elitismCount)
                {
                    Individual parent2 = selection.select(population);
                    int[] offspringChromosome = new int[parent1.getChromosomeLength()];
                    for (int i = 0; i < offspringChromosome.Length; i++)
                        offspringChromosome[i] = -1;
                    Individual offspring = new Individual(offspringChromosome);

                    int substrPos1 = (int)(rnd.NextDouble() * parent1.getChromosomeLength());
                    int substrPos2 = (int)(rnd.NextDouble() * parent1.getChromosomeLength());

                    int startSubstr = Math.Min(substrPos1, substrPos2);
                    int endSubstr = Math.Max(substrPos1, substrPos2);

                    for (int i = startSubstr; i < endSubstr; i++)
                        offspring.setGene(i, parent1.getGene(i));

                    for (int i = 0; i < parent2.getChromosomeLength(); i++)
                    {
                        int parent2Gene = i + endSubstr;
                        if (parent2Gene >= parent2.getChromosomeLength())
                            parent2Gene -= parent2.getChromosomeLength();

                        if (offspring.containsGene(parent2.getGene(parent2Gene)) == false)
                        {
                            for (int ii = 0; ii < offspring.getChromosomeLength(); ii++)
                                if (offspring.getGene(ii) == -1)
                                {
                                    offspring.setGene(ii, parent2.getGene(parent2Gene));
                                    break;
                                }
                        }
                    }
                    newPopulation.setIndividual(populationIndex, offspring);
                }
                else
                    newPopulation.setIndividual(populationIndex, parent1);
            }
            return newPopulation;
        }
    }
}
