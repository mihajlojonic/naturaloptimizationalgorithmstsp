﻿using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Corossovers
{
    class SinglePointCrossover : ICrossover
    {
        private int elitismCount;
        private double crossoverRate;
        private ISelection selection;

        public SinglePointCrossover(int elitismCount, double crossoverRate, ISelection selection)
        {
            this.elitismCount = elitismCount;
            this.crossoverRate = crossoverRate;
            this.selection = selection;
        }

        public Population crossover(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual parent1 = population.getFittest(populationIndex);

                if (this.crossoverRate > rnd.NextDouble() && populationIndex > this.elitismCount)
                {
                    Individual offspring = new Individual(parent1.getChromosomeLength());
                    Individual parent2 = selection.select(population);
                    int swapPoint = (int)(rnd.NextDouble() * (parent1.getChromosomeLength() + 1));

                    for (int geneIndex = 0; geneIndex < parent1.getChromosomeLength(); geneIndex++)
                        if (geneIndex < swapPoint)
                            offspring.setGene(geneIndex, parent1.getGene(geneIndex));
                        else
                            offspring.setGene(geneIndex, parent2.getGene(geneIndex));

                    offspring = this.validateIndividual(offspring);
                    newPopulation.setIndividual(populationIndex, offspring);
                }
                else
                    newPopulation.setIndividual(populationIndex, parent1);
            }

            return newPopulation;
        }

        private Individual validateIndividual(Individual individual)
        {
            for (int gene = 0; gene < individual.getChromosomeLength(); gene++)
            {
                if (individual.containsGene(gene) == false)
                {
                    HashSet<Int32> set = new HashSet<Int32>();
                    for (int i = 0; i < individual.getChromosomeLength(); i++)
                    {
                        if (set.Add(individual.Chromosome[i]) == false)
                        {
                            individual.setGene(i, gene);
                        }
                    }
                }
            }

            return individual;
        }
    }
}
