﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Corossovers
{
    interface ICrossover
    {
        Population crossover(Population population);
    }
}
