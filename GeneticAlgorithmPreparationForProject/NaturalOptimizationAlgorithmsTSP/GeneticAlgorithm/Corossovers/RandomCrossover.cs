﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Corossovers
{
    class RandomCrossover : ICrossover
    {
        private int elitismCount;
        private double crossoverRate;
        private ISelection selection;

        public RandomCrossover(int elitismCount, double crossoverRate, ISelection selection)
        {
            this.elitismCount = elitismCount;
            this.crossoverRate = crossoverRate;
            this.selection = selection;
        }

        public Population crossover(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual parent1 = population.getFittest(populationIndex);

                if (this.crossoverRate > rnd.NextDouble() && populationIndex > this.elitismCount)
                {
                    Individual offspring = new Individual(parent1.getChromosomeLength());
                    Individual parent2 = selection.select(population);

                    for (int geneIndex = 0; geneIndex < parent1.getChromosomeLength(); geneIndex++)
                    {
                        if (0.5 > rnd.NextDouble() || offspring.containsGene(parent2.getGene(geneIndex)))
                        {
                            if (offspring.containsGene(parent1.getGene(geneIndex)) == false)
                                offspring.setGene(geneIndex, parent1.getGene(geneIndex));
                            else
                                offspring.setGene(geneIndex, parent2.getGene(geneIndex));
                        }
                        else
                        {
                            offspring.setGene(geneIndex, parent2.getGene(geneIndex));
                        }
                    }
                }
            }

            return newPopulation;
        }
    }
}
