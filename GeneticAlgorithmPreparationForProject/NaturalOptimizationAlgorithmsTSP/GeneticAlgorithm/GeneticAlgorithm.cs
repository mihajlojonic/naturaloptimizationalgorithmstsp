﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Mutations;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Corossovers;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm
{
    class GeneticAlgorithm
    {
        private int generationCount = 1;
        public static int maxGenerations = 3000;
        public static double optimalFitness = 0.99;

        private int populationSize;
        private double mutationRate;
        private double crossoverRate;
        private int elitismCount;
        protected int turnamentSize;
        private IMutation mutation;
        private ICrossover crossover;
        private ISelection selection;

        public GeneticAlgorithm(int populationSize, double mutationRate, double crossoverRate, int elitismCount, int turnamentSize)
        {
            this.populationSize = populationSize;
            this.mutationRate = mutationRate;
            this.crossoverRate = crossoverRate;
            this.elitismCount = elitismCount;
            this.turnamentSize = turnamentSize;

            mutation = new SwapMutation(elitismCount, mutationRate);
            crossover = new OrderedCrossover(elitismCount, crossoverRate, selection);
            selection = new TurnamentSelection(turnamentSize);
        }

        public GeneticAlgorithm(int populationSize, double mutationRate, double crossoverRate, int elitismCount, int turnamentSize, IMutation mut, ICrossover cross, ISelection sele)
        {
            this.populationSize = populationSize;
            this.mutationRate = mutationRate;
            this.crossoverRate = crossoverRate;
            this.elitismCount = elitismCount;
            this.turnamentSize = turnamentSize;

            mutation = mut;
            crossover = cross;
            selection = sele;
        }

        public void run()
        {
            /*
            int numCities = 100;
            City[] cities = new City[numCities];

            Random rnd = new Random();
            for (int cityIndex = 0; cityIndex < numCities; cityIndex++)
            {
                int xPos = rnd.Next(100);
                int yPos = rnd.Next(100);
                cities[cityIndex] = new City(xPos, yPos);
            }

            Population population = this.initPopulation(cities.Length);
            this.evalPopulation(population, cities);

            while (this.isTerminateConditionMet(generationCount, maxGenerations) == false)
            {
                Route route = new Route(population.getFittest(0), cities);
                Console.WriteLine("G: " + generationCount + " Best distance: " + route.getDistance());

                population = this.crossoverPopulation(population);
                population = this.mutatePopulation(population);

                this.evalPopulation(population, cities);

                generationCount++;
            }
            Console.WriteLine("Stopped after: " + maxGenerations + " generations.");
            Route routeBest = new Route(population.getFittest(0), cities);
            Console.WriteLine("Best distance: " + routeBest.getDistance());
            */
        }

        public Population initPopulation(int chromosomeLength, int countOfSpecialInitialize)
        {
            Population population = new Population(this.populationSize, chromosomeLength, countOfSpecialInitialize);
            return population;
        }

        public double calcFitness(Individual individual, City[] cities)
        {
            Route route = new Route(individual, cities);
            double fitness = 1 / route.getDistance();
            individual.Fitness = fitness;
            return fitness;
        }

        public void evalPopulation(Population population, City[] cities)
        {
            double populationFitness = 0;

            foreach (Individual individual in population.PopulationArray)
                populationFitness += this.calcFitness(individual, cities);

            double avgFitness = populationFitness / population.size();
            population.PopulationFitness = avgFitness;
        }

        public bool isTerminateConditionMet(int generationCount, int maxGenerations)
        {
            return (generationCount > maxGenerations);
        }

        public Individual selectParent(Population population)
        {
            return selection.select(population);
            /*
            Population tournament = new Population(this.turnamentSize);
            population.shuffle();
            for (int i = 0; i < this.turnamentSize; i++)
            {
                Individual tournamentIndividual = population.getIndividual(i);
                tournament.setIndividual(i, tournamentIndividual);
            }

            return tournament.getFittest(0);*/
        }

        
        public Population crossoverPopulation(Population population)
        {
            return crossover.crossover(population);
            /*
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual parent1 = population.getFittest(populationIndex);

                if (this.crossoverRate > rnd.NextDouble() && populationIndex >= elitismCount)
                {
                    Individual parent2 = this.selectParent(population);
                    int[] offspringChromosome = new int[parent1.getChromosomeLength()];
                    for (int i = 0; i < offspringChromosome.Length; i++)
                        offspringChromosome[i] = -1;
                    Individual offspring = new Individual(offspringChromosome);

                    int substrPos1 = (int)(rnd.NextDouble() * parent1.getChromosomeLength());
                    int substrPos2 = (int)(rnd.NextDouble() * parent1.getChromosomeLength());

                    int startSubstr = Math.Min(substrPos1, substrPos2);
                    int endSubstr = Math.Max(substrPos1, substrPos2);

                    for (int i = startSubstr; i < endSubstr; i++)
                        offspring.setGene(i, parent1.getGene(i));

                    for (int i = 0; i < parent2.getChromosomeLength(); i++)
                    {
                        int parent2Gene = i + endSubstr;
                        if (parent2Gene >= parent2.getChromosomeLength())
                            parent2Gene -= parent2.getChromosomeLength();

                        if (offspring.containsGene(parent2.getGene(parent2Gene)) == false)
                        {
                            for (int ii = 0; ii < offspring.getChromosomeLength(); ii++)
                                if (offspring.getGene(ii) == -1)
                                {
                                    offspring.setGene(ii, parent2.getGene(parent2Gene));
                                    break;
                                }
                        }
                    }
                    newPopulation.setIndividual(populationIndex, offspring);
                }
                else
                    newPopulation.setIndividual(populationIndex, parent1);
            }
            return newPopulation;*/
        }

        public Population mutatePopulation(Population population)
        {
            return mutation.mutate(population);
            /*
            Population newPopulation = new Population(this.populationSize);
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual individual = population.getFittest(populationIndex);

                if (populationIndex >= this.elitismCount)
                {
                    for (int geneIndex = 0; geneIndex < individual.getChromosomeLength(); geneIndex++)
                    {
                        if (this.mutationRate > rnd.NextDouble())
                        {

                            int newGenPos = (int)(rnd.NextDouble() * individual.getChromosomeLength());
                            int gene1 = individual.getGene(newGenPos);
                            int gene2 = individual.getGene(geneIndex);

                            individual.setGene(geneIndex, gene1);
                            individual.setGene(newGenPos, gene2);
                        }
                    }
                }
                newPopulation.setIndividual(populationIndex, individual);
            }
            return newPopulation;*/
        }
    }
}
