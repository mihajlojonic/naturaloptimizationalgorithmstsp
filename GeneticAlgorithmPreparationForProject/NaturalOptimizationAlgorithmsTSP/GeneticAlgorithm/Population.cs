﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm
{
    class Population
    {
        private Individual[] populationArray;
        private double populationFitness = -1;

        public Individual[] PopulationArray { get => populationArray; set => populationArray = value; }
        public double PopulationFitness { get => populationFitness; set => populationFitness = value; }

        public Population(int populationSize)
        {
            this.PopulationArray = new Individual[populationSize];
        }

        public Population(int populationSize, int chromosomeLength, int countOfSpecialInitialize)
        {
            this.PopulationArray = new Individual[populationSize];
            for (int individualCount = 0; individualCount < populationSize; individualCount++)
            {
                if(individualCount < countOfSpecialInitialize)
                {
                    Individual individual = new Individual(chromosomeLength);
                    this.PopulationArray[individualCount] = individual;
                }
                else
                {
                    Individual individual = new Individual(chromosomeLength, true);
                    this.PopulationArray[individualCount] = individual;
                }         
            }
        }


        public Individual getFittest(int offset)
        {
            sortPopulationByFitness();
            return this.PopulationArray[offset];
        }

        public void sortPopulationByFitness()
        {
            int i, j, max_idx;

            for (i = 0; i < this.PopulationArray.Length; i++)
            {
                max_idx = i;
                for (j = i + 1; j < this.PopulationArray.Length; j++)
                    if (PopulationArray[j].Fitness > PopulationArray[max_idx].Fitness)
                        max_idx = j;

                Individual temp = this.PopulationArray[max_idx];
                this.PopulationArray[max_idx] = this.PopulationArray[i];
                this.PopulationArray[i] = temp;
            }
        }


        public int size()
        {
            return this.PopulationArray.Length;
        }

        public Individual setIndividual(int offset, Individual individual)
        {
            return PopulationArray[offset] = individual;
        }

        public Individual getIndividual(int offset)
        {
            return PopulationArray[offset];
        }

        public void shuffle()
        {
            Random rnd = new Random();
            for (int i = PopulationArray.Length - 1; i > 0; i--)
            {
                int index = rnd.Next(i + 1);
                Individual a = PopulationArray[index];
                PopulationArray[index] = PopulationArray[i];
                PopulationArray[i] = a;
            }
        }
    }
}
