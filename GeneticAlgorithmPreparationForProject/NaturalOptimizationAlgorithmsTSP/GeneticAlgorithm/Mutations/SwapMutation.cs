﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Mutations
{
    class SwapMutation : IMutation
    {
        private int elitismCount;
        private double mutationRate;

        public SwapMutation(int elitism, double mutate)
        {
            this.elitismCount = elitism;
            this.mutationRate = mutate;
        }

        public Population mutate(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual individual = population.getFittest(populationIndex);

                if (populationIndex >= this.elitismCount)
                {
                    for (int geneIndex = 0; geneIndex < individual.getChromosomeLength(); geneIndex++)
                    {
                        if (this.mutationRate > rnd.NextDouble())
                        {
                            int newGenPos = (int)(rnd.NextDouble() * individual.getChromosomeLength());
                            int gene1 = individual.getGene(newGenPos);
                            int gene2 = individual.getGene(geneIndex);

                            individual.setGene(geneIndex, gene1);
                            individual.setGene(newGenPos, gene2);
                        }
                    }
                }
                newPopulation.setIndividual(populationIndex, individual);
            }
            return newPopulation;
        }
    }
}
