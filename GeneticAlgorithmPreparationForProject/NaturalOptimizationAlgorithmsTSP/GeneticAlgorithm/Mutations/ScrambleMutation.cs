﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Mutations
{
    class ScrambleMutation : IMutation
    {
        int elitismCount;
        double mutationRate;

        public ScrambleMutation(int elitism, double mutation)
        {
            this.elitismCount = elitism;
            this.mutationRate = mutation;
        }

        public Population mutate(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual individual = population.getFittest(populationIndex);

                if (populationIndex >= this.elitismCount && mutationRate > rnd.NextDouble())
                {
                    int firstPoint = rnd.Next(individual.getChromosomeLength());
                    int secondPoint = rnd.Next(firstPoint + 1, individual.getChromosomeLength());

                    int[] firstPart = individual.Chromosome.Skip(0).Take(firstPoint).ToArray();
                    int[] shufflePart = individual.Chromosome.Skip(firstPoint).Take(secondPoint - firstPoint).ToArray();
                    int[] secondPart = individual.Chromosome.Skip(secondPoint).Take(individual.getChromosomeLength() - secondPoint).ToArray();

                    shufflePart = shuffle(shufflePart);

                    individual.Chromosome = firstPart.Concat(shufflePart).Concat(secondPart).ToArray();
                }

                newPopulation.setIndividual(populationIndex, individual);
            }
            return newPopulation;
        }

        private int[] shuffle(int[] shuffleArray)
        {
            Random rnd = new Random();
            for (int i = shuffleArray.Length - 1; i > 0; i--)
            {
                int index = rnd.Next(i + 1);
                int a = shuffleArray[index];
                shuffleArray[index] = shuffleArray[i];
                shuffleArray[i] = a;
            }

            return shuffleArray;
        }
    }
}
