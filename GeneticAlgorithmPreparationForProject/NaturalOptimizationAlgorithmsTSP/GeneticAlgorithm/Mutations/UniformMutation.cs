﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Mutations
{
    class UniformMutation : IMutation
    {
        int elitismCount;
        double mutationRate;

        public UniformMutation(int elitism, double mutation)
        {
            this.elitismCount = elitism;
            this.mutationRate = mutation;
        }

        public Population mutate(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual individual = population.getFittest(populationIndex);
                Individual randomIndividual = new Individual(individual.getChromosomeLength());

                if (populationIndex >= this.elitismCount)
                    for (int geneIndex = 0; geneIndex < individual.getChromosomeLength(); geneIndex++)
                        if (this.mutationRate > rnd.NextDouble())
                            individual.setGene(geneIndex, randomIndividual.getGene(geneIndex));

                individual = this.validateIndividual(individual);
                newPopulation.setIndividual(populationIndex, individual);
            }
            return newPopulation;
        }

        private Individual validateIndividual(Individual individual)
        {
            for (int gene = 0; gene < individual.getChromosomeLength(); gene++)
            {
                if (individual.containsGene(gene) == false)
                {
                    HashSet<Int32> set = new HashSet<Int32>();
                    for (int i = 0; i < individual.getChromosomeLength(); i++)
                    {
                        if (set.Add(individual.Chromosome[i]) == false)
                        {
                            individual.setGene(i, gene);
                        }
                    }
                }
            }

            return individual;
        }
    }
}
