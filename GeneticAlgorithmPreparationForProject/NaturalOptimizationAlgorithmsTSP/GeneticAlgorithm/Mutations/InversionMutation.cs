﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Mutations
{
    class InversionMutation : IMutation
    {
        int elitismCount;
        double mutationRate;

        public InversionMutation(int elitism, double mutation)
        {
            this.elitismCount = elitism;
            this.mutationRate = mutation;
        }

        public Population mutate(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for (int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual individual = population.getFittest(populationIndex);

                if (populationIndex >= this.elitismCount && mutationRate > rnd.NextDouble())
                {
                    int firstPoint = rnd.Next(individual.getChromosomeLength());
                    int secondPoint = rnd.Next(firstPoint + 1, individual.getChromosomeLength());

                    int[] firstPart = individual.Chromosome.Skip(0).Take(firstPoint).ToArray();
                    int[] inversionPart = individual.Chromosome.Skip(firstPoint).Take(secondPoint - firstPoint).ToArray();
                    int[] secondPart = individual.Chromosome.Skip(secondPoint).Take(individual.getChromosomeLength() - secondPoint).ToArray();

                    inversionPart = inversionPart.Reverse().ToArray();

                    individual.Chromosome = firstPart.Concat(inversionPart).Concat(secondPart).ToArray();
                }

                newPopulation.setIndividual(populationIndex, individual);
            }
            return newPopulation;
        }
    }
}
