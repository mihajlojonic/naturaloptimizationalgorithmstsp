﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections
{
    class RankSelection : ISelection
    {
        public RankSelection() { }

        public Individual select(Population population)
        {
            int size = population.size() / 5;
            Population newPopulation = new Population(size);
            int currentSize = population.size();

            population.sortPopulationByFitness();
            //"sum of ranks"
            double ranges = currentSize * (currentSize + 1) / 2;

            double[] rangeMax = new double[currentSize];
            double s = 0;

            for (int i = 0, n = currentSize; i < currentSize; i++, n--)
            {
                s += ((double)n / ranges);
                rangeMax[i] = s;
            }

            Random rnd = new Random();
            for (int i = 0; i < size; i++)
            {
                double wheelValue = rnd.NextDouble();

                for (int j = 0; j < currentSize; j++)
                    if (wheelValue <= rangeMax[j])
                    {
                        newPopulation.setIndividual(i, population.getIndividual(j));
                        break;
                    }
            }

            return newPopulation.getIndividual(rnd.Next(size));

        }
    }
}
