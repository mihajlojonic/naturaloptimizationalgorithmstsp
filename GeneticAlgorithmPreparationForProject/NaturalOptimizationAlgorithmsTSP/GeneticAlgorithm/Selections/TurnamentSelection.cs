﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections
{
    class TurnamentSelection : ISelection
    {
        public int turnamentSize;

        public TurnamentSelection(int turnamentSize)
        {
            this.turnamentSize = turnamentSize;
        }

        public Individual select(Population population)
        {
            Population tournament = new Population(this.turnamentSize);
            population.shuffle();
            for (int i = 0; i < this.turnamentSize; i++)
            {
                Individual tournamentIndividual = population.getIndividual(i);
                tournament.setIndividual(i, tournamentIndividual);
            }

            return tournament.getFittest(0);
        }
    }
}
