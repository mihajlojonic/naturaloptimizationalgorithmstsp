﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections
{
    class EliteSelection : ISelection
    {
        public Individual select(Population population)
        {
            return population.getFittest(0);
        }
    }
}
