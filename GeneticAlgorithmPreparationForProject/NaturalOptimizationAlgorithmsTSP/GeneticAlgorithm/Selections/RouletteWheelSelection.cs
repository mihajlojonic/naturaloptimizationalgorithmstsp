﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections
{
    class RouletteWheelSelection : ISelection
    {
        public RouletteWheelSelection() { }

        public Individual select(Population population)
        {
            Individual[] individuals = population.PopulationArray;
            Random rnd = new Random();
            double populationFitness = population.PopulationFitness;
            double rouletteWheelPosition = rnd.NextDouble() * populationFitness;

            double spinWheel = 0;
            foreach (Individual individual in individuals)
            {
                spinWheel += individual.Fitness;
                if (spinWheel >= rouletteWheelPosition)
                    return individual;
            }
            return individuals[population.size() - 1];
        }
    }
}
