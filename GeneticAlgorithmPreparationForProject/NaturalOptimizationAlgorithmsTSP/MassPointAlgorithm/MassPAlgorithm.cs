﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.MassPointAlgorithm
{
    public class MassPAlgorithm
    {
        City[] cities;
        City massPointCIty = new City(0.0, 0.0);

        public MassPAlgorithm(City[] cities) { this.Cities = cities; }

        internal City[] Cities { get => cities; set => cities = value; }
        public City MassPointCIty { get => massPointCIty; set => massPointCIty = value; }

        private void calculateMassPoint()
        {
            for (int i = 0; i < Cities.Length - 1; i++)
            {
                MassPointCIty.X += Cities[i].X;
                MassPointCIty.Y += Cities[i].Y;
            }
            MassPointCIty.X = MassPointCIty.X / Cities.Length;
            MassPointCIty.Y = MassPointCIty.Y / Cities.Length;
        }

        public City[] Slove()
        {
            this.calculateMassPoint();
            double[] distances = new double[Cities.Length];

            for (int i = 0; i < Cities.Length; i++)
                distances[i] = MassPointCIty.distanceFrom(Cities[i]);

            for(int i = 0; i < Cities.Length - 1; i++)
            {
                int minIndex = i;
                for (int j = i + 1; j < Cities.Length; j++)
                    if(distances[j] < distances[minIndex])
                        minIndex = j;

                double tempDist = distances[minIndex];
                distances[minIndex] = distances[i];
                distances[i] = tempDist;

                City tempCity = Cities[minIndex];
                Cities[minIndex] = Cities[i];
                Cities[i] = tempCity;
            }

            return Cities;
        }
    }
}
