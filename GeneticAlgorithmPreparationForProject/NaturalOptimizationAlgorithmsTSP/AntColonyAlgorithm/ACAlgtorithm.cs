﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.AntColonyAlgorithm
{
    class ACAlgtorithm
    {
        Random rnd = new Random();
        int alpha = 3;
        int beta = 2;
        double rho = 0.01;
        double Q = 2.0;

        int numCities = 60;
        int numAnts = 4;
        int maxTime = 1000;
        int elitismCount;
        Colony colony;

        double[][] distances;
        double[][] pheromones;

        internal Colony Colony { get => Colony1; set => Colony1 = value; }
        public double[][] Distances { get => Distances1; set => Distances1 = value; }
        public double[][] Pheromones { get => Pheromones1; set => Pheromones1 = value; }
        public int MaxTime { get => MaxTime1; set => MaxTime1 = value; }
        public int Alpha { get => alpha; set => alpha = value; }
        public int Beta { get => beta; set => beta = value; }
        public double Rho { get => rho; set => rho = value; }
        public double Q1 { get => Q; set => Q = value; }
        public int NumCities { get => numCities; set => numCities = value; }
        public int NumAnts { get => numAnts; set => numAnts = value; }
        public int MaxTime1 { get => maxTime; set => maxTime = value; }
        internal Colony Colony1 { get => colony; set => colony = value; }
        public double[][] Distances1 { get => distances; set => distances = value; }
        public double[][] Pheromones1 { get => pheromones; set => pheromones = value; }

        public ACAlgtorithm(int numCities, int numAnts, int maxTime, int elitismCount)
        {
            this.NumCities = numCities;
            this.NumAnts = numAnts;
            this.MaxTime = maxTime;
            this.elitismCount = elitismCount;

        }

        public void MakeGraphDistances(City[] cities)
        {
            this.Distances = new double[NumCities][];

            for (int i = 0; i < Distances.Length; i++)
                Distances[i] = new double[NumCities];

            for (int i = 0; i < NumCities; i++)
            {
                for (int j = i + 1; j < NumCities; j++)
                {
                    double d = cities[i].distanceFrom(cities[j]);

                    Distances[i][j] = Distances[j][i] = d;
                }
            }
        }

        public void InitColony()
        {
            this.Colony = new Colony(this.NumAnts, this.NumCities, this.Distances, this.elitismCount);
        }

        public double calcFitness(Ant ant, City[] cities)
        {
            Route route = new Route(ant, cities);
            double fitness = 1 / route.getDistance();
            ant.Fitness = fitness;
            return fitness;
        }

        public void evalColony(City[] cities)
        {
            double colonyFitness = 0;

            foreach (Ant ant in Colony1.AntsArray)
                colonyFitness += this.calcFitness(ant, cities);

            double avgFitness = colonyFitness / Colony1.AntsArray.Count();
            Colony1.ColonyFitness = avgFitness;
        }

        public void initPheromones()
        {
            this.Pheromones1 = new double[NumCities][];
            for (int i = 0; i < NumCities; i++)
                Pheromones1[i] = new double[NumCities];

            for (int i = 0; i < Pheromones1.Length; i++)
                for (int j = 0; i < Pheromones1.Length; i++)
                    this.Pheromones1[i][j] = 0.01;
        }

        public void updateColony()
        {
            for (int k = 0; k < Colony1.AntsArray.Length; k++)
            {
                if (k > elitismCount)
                {
                    int start = rnd.Next(0, NumCities);
                    int[] newTrail = BuildTrail(k, start);
                    Colony1.AntsArray[k].AntTrail = newTrail;
                }
            }
        }

        private int[] BuildTrail(int k, int start)
        {
            int[] trail = new int[NumCities];
            bool[] visited = new bool[NumCities];
            trail[0] = start;
            visited[start] = true;
            for (int i = 0; i < NumCities - 1; i++)
            {
                int cityX = trail[i];
                int next = NextCity(k, cityX, visited);
                trail[i + 1] = next;
                visited[next] = true;
            }
            return trail;
        }

        private int NextCity(int k, int cityX, bool[] visited)
        {
            double[] probs = MoveProbs(k, cityX, visited);

            double[] cumul = new double[probs.Length + 1];
            for (int i = 0; i < probs.Length; i++)
                cumul[i + 1] = cumul[i] + probs[i];

            double p = rnd.NextDouble();

            for (int i = 0; i < cumul.Length - 1; i++)
                if (p >= cumul[i] && p < cumul[i + 1])
                    return i;
            throw new Exception("Failure to return valid city in NextCity");
        }

        private double[] MoveProbs(int k, int cityX, bool[] visited)
        {
            double[] taueta = new double[NumCities];
            double sum = 0.0;

            for (int i = 0; i < taueta.Length; i++)
            {
                if (i == cityX)
                {
                    taueta[i] = 0.0;
                }
                else if (visited[i] == true)
                {
                    taueta[i] = 0.0;
                }
                else
                {
                    taueta[i] = Math.Pow(Pheromones1[cityX][i], Alpha) * Math.Pow(1.0 / distances[cityX][i], Beta);
                    if (taueta[i] < 0.0001)
                        taueta[i] = 0.0001;
                    else if (taueta[i] > (double.MaxValue / (NumCities * 100)))
                        taueta[i] = double.MaxValue / (NumCities * 100);
                }
                sum += taueta[i];
            }

            double[] probs = new double[NumCities];
            for (int i = 0; i < probs.Length; i++)
                probs[i] = taueta[i] / sum;

            return probs;
        }


        public void updatePheromones()
        {
            for (int i = 0; i < Pheromones1.Length; i++)
                for (int j = i + 1; j < Pheromones1[i].Length; j++)
                    for (int k = 0; k < Colony1.AntsArray.Length; k++)
                    {
                        double length = Length(Colony1.AntsArray[k].AntTrail);

                        double decrease = (1.0 - Rho) * Pheromones1[i][j];
                        double increse = 0.0;
                        if (EdgeInTrail(i, j, Colony1.AntsArray[k].AntTrail) == true)
                        {
                            increse = (Q1 / length);
                        }

                        Pheromones1[i][j] = decrease + increse;

                        if (Pheromones1[i][j] < 0.0001)
                            Pheromones1[i][j] = 0.0001;
                        else if (Pheromones1[i][j] > 100000.0)
                            Pheromones1[i][j] = 100000.0;

                        Pheromones1[j][i] = Pheromones1[i][j];
                    }
        }

        private bool EdgeInTrail(int cityX, int cityY, int[] trail)
        {
            int lastIndex = trail.Length - 1;
            int idx = IndexOfTarget(trail, cityX);

            if (idx == 0 && trail[1] == cityY)
                return true;
            else if (idx == 0 && trail[lastIndex] == cityY)
                return true;
            else if (idx == 0)
                return false;
            else if (idx == lastIndex && trail[lastIndex - 1] == cityY)
                return true;
            else if (idx == lastIndex && trail[0] == cityY)
                return true;
            else if (idx == lastIndex)
                return false;
            else if (trail[idx - 1] == cityY)
                return true;
            else if (trail[idx + 1] == cityY)
                return true;
            else
                return false;
        }

        private int IndexOfTarget(int[] trail, int cityX)
        {
            for (int i = 0; i < trail.Length; i++)
                if (trail[i] == cityX)
                    return i;
            throw new Exception("Target not found in IndexOfTarget");
        }

        private double Length(int[] antTrail)
        {
            double result = 0.0;

            for (int i = 0; i < antTrail.Length - 1; i++)
                result += distances[antTrail[i]][antTrail[i + 1]];

            return result;
        }
    }
}
