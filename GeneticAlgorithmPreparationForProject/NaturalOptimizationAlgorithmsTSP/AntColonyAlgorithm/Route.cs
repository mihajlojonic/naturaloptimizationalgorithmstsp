﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.AntColonyAlgorithm
{
    class Route
    {
        public City[] route;
        private double distance = 0;

        public Route(Ant ant, City[] cities)
        {
            int[] trail = ant.AntTrail;
            this.route = new City[cities.Length];
            for (int trailIndex = 0; trailIndex < trail.Length; trailIndex++)
                this.route[trailIndex] = cities[trail[trailIndex]];
        }

        public double getDistance()
        {
            if (this.distance > 0)
                return this.distance;

            double totalDistance = 0;
            for (int cityIndex = 0; cityIndex + 1 < this.route.Length; cityIndex++)
                totalDistance += this.route[cityIndex].distanceFrom(this.route[cityIndex + 1]);

            totalDistance += this.route[this.route.Length - 1].distanceFrom(this.route[0]);
            this.distance = totalDistance;

            return totalDistance;
        }
    }
}
