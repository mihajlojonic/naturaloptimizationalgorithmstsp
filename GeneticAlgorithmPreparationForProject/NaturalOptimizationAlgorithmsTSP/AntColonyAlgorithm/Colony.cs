﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.AntColonyAlgorithm
{
    class Colony
    {
        Random rnd = new Random();
        private Ant[] antsArray;
        private double colonyFitness;
        private double[][] distances;
        int elitismCount;
        int numCities;

        internal Ant[] AntsArray { get => antsArray; set => antsArray = value; }
        public double ColonyFitness { get => colonyFitness; set => colonyFitness = value; }

        public Colony(int colonySize, int trailLength, double[][] distances, int elitism)
        {
            this.numCities = trailLength;
            this.elitismCount = elitism;
            this.distances = distances;
            this.AntsArray = new Ant[colonySize];
            for (int antIndex = 0; antIndex < colonySize; antIndex++)
            {
                Ant ant = new Ant(trailLength);
                this.AntsArray[antIndex] = ant;
            }
        }

        internal Ant GetFittest(int offset)
        {
            sortColonyByFitness();
            return this.AntsArray[offset];
        }

        private void sortColonyByFitness()
        {
            int i, j, maxIDx;

            for (i = 0; i < this.AntsArray.Length; i++)
            {
                maxIDx = i;
                for (j = i + 1; j < this.AntsArray.Length; j++)
                    if (AntsArray[j].Fitness > AntsArray[maxIDx].Fitness)
                        maxIDx = j;

                Ant temp = this.AntsArray[maxIDx];
                this.AntsArray[maxIDx] = this.AntsArray[i];
                this.AntsArray[i] = temp;
            }
        }
    }
}
