﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.AntColonyAlgorithm
{
    class Ant
    {
        private int[] antTrail;
        private double fitness = -1;

        public int[] AntTrail { get => antTrail; set => antTrail = value; }
        public double Fitness { get => fitness; set => fitness = value; }

        public Ant(int trailLength, bool tmp)
        {
            int[] trail;
            trail = new int[trailLength];

            for (int cityNum = 0; cityNum < antTrail.Length; cityNum++)
                AntTrail[cityNum] = cityNum;

            this.AntTrail = trail;
        }

        public Ant(int trailLength)
        {
            this.AntTrail = new int[trailLength];
            Random rnd = new Random();
            int startCity = rnd.Next(0, trailLength);

            for (int i = 0; i < trailLength; i++)
                AntTrail[i] = i;

            for (int shuffleIndex = 0; shuffleIndex < trailLength; shuffleIndex++)
            {
                int randomTemp = rnd.Next(shuffleIndex, trailLength);
                int tmp = AntTrail[randomTemp];
                AntTrail[randomTemp] = AntTrail[shuffleIndex];
                AntTrail[shuffleIndex] = tmp;
            }

            int indexOfTarget = IndexOfTarget(startCity);

            int temp = AntTrail[0];
            AntTrail[0] = AntTrail[indexOfTarget];
            AntTrail[indexOfTarget] = temp;
        }

        private int IndexOfTarget(int startCity)
        {
            for (int i = 0; i < AntTrail.Length; i++)
                if (AntTrail[i] == startCity)
                    return i;
            throw new Exception("Target not found in IndexOfTarget");
        }
    }
}
