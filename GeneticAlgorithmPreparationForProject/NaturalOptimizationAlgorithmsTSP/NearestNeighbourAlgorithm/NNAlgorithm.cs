﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoreLinq;
using NaturalOptimizationAlgorithmsTSP;

namespace NaturalOptimizationAlgorithmsTSP.NearestNeighbourAlgorithm
{ 
    public class NNAlgorithm
    {
        City[] cities;

        public NNAlgorithm(City[] cities) { this.Cities = cities; }

        internal City[] Cities { get => cities; set => cities = value; }

        public City[] Slove()
        {
            for (int i = 0; i < Cities.Length - 1; i++)
            {
                int nearestIndex = i + 1;
                double nearestDistance = Cities[i].distanceFrom(Cities[nearestIndex]);
                for (int j = i + 2; j < Cities.Length; j++)
                {
                    double localDistance = Cities[i].distanceFrom(Cities[j]);
                    if (nearestDistance > localDistance)
                    {
                        nearestDistance = localDistance;
                        nearestIndex = j;
                    }
                }

                City swapCity = Cities[nearestIndex];
                Cities[nearestIndex] = Cities[i + 1];
                Cities[i + 1] = swapCity;
            }

            return Cities;
        }
    }
}
