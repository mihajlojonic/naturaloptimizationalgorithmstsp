﻿using System;

namespace NaturalOptimizationAlgorithmsTSP.SimulatedAnnealingAlgorithm
{
    class Route
    {
        public City[] route;
        private City[] previousRoute;
        private double distance = 0.0;

        public City[] PreviousRoute { get => previousRoute; set => previousRoute = value; }

        public Route(City[] cities)
        {
            route = new City[cities.Length];
            PreviousRoute = new City[cities.Length];

            for (int cityIndex = 0; cityIndex < cities.Length; cityIndex++)
                route[cityIndex] = cities[cityIndex];
        }

        public void swapCities()
        {
            Random rnd = new Random();
            int a = rnd.Next(0, route.Length);
            int b = rnd.Next(0, route.Length);
            PreviousRoute = route;
            City x = route[a];
            City y = route[b];
            route[a] = y;
            route[b] = x;
        }

        public void revertSwap()
        {
            route = PreviousRoute;
        }

        public double getDistance()
        {
            if (this.distance > 0)
                return this.distance;

            double totalDistance = 0;
            for (int cityIndex = 0; cityIndex + 1 < this.route.Length; cityIndex++)
                totalDistance += this.route[cityIndex].distanceFrom(this.route[cityIndex + 1]);

            totalDistance += this.route[this.route.Length - 1].distanceFrom(this.route[0]);
            this.distance = totalDistance;

            return totalDistance;
        }
    }
}
