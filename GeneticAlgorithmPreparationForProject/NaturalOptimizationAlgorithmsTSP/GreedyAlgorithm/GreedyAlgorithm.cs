﻿using NaturalOptimizationAlgorithmsTSP.CustomControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.GreedyAlgorithm
{
    public struct Distance
    {
        public City cityX;
        public double distance;
        public City cityY;

        public Distance(City X, double d, City Y)
        {
            this.cityX = X;
            this.cityY = Y;
            this.distance = d;
        }
    }

    public class GreedyAlgorithm
    {
        City[] cities;

        List<Distance> distances = new List<Distance>();

        public GreedyAlgorithm(City[] cities) { this.Cities = cities; }

        internal City[] Cities { get => cities; set => cities = value; }
        public List<Distance> Distances { get => distances; set => distances = value; }

        private void calculateAndSortDistances()
        {
            for (int i = 0; i < Cities.Length - 1; i++)
            {
                for (int j = i + 1; j < Cities.Length; j++)
                {
                    double distance = Cities[i].distanceFrom(Cities[j]);
                    this.Distances.Add(new Distance(Cities[i], distance, Cities[j]));
                }
            }

            Distances.Sort((a, b) => (a.distance.CompareTo(b.distance)));
        }

        public City[] Slove()
        {
            this.calculateAndSortDistances();
            List<City> tmpCities = new List<City>();

            for(int i = 1; i < Cities.Length - 1; i++)
            {
                List<Distance> tmpDistances = Distances.Where(dist => dist.cityX == Cities[i]).ToList();
                tmpDistances.Sort((a, b) => (a.distance.CompareTo(b.distance)));
                //tmpDistances.Reverse();

                if(!tmpCities.Contains(tmpDistances.ElementAt(0).cityX))
                {
                    tmpCities.Add(tmpDistances.ElementAt(0).cityX);
                    cities[i] = tmpDistances.ElementAt(0).cityX;
                }
            }

            return Cities;
        }
    }
}
