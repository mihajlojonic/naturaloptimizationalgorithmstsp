﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP
{
    public class City : IComparable<City>
    {
        private double x;
        private double y;

        public double X { get => x; set => x = value; }
        public double Y { get => y; set => y = value; }

        public City(double x, double y)
        {
            this.X = x;
            this.Y = y;
        }

        public City()
        {
        }

        public double distanceFrom(City city)
        {
            double deltaXSq = Math.Pow((city.X - this.X), 2);
            double deltaYSq = Math.Pow((city.Y - this.Y), 2);

            double distance = Math.Sqrt(Math.Abs(deltaXSq + deltaYSq));
            return distance;
        }

        public int CompareTo(City other)
        {
            if (x < other.x)
                return -1;
            else if (x > other.x)
                return +1;
            else if (y < other.y)
                return -1;
            else if (y > other.y)
                return +1;
            else
                return 0;
        }
    }
}
