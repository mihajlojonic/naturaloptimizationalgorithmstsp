﻿using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP
{
    public class Route
    {
        public City[] route;
        private double distance = 0;

        public Route(Individual individual, City[] cities)
        {
            int[] chromosome = individual.Chromosome;
            this.route = new City[cities.Length];
            for (int geneIndex = 0; geneIndex < chromosome.Length; geneIndex++)
                this.route[geneIndex] = cities[chromosome[geneIndex]];
        }

        public double getDistance()
        {
            if (this.distance > 0)
                return this.distance;

            double totalDistance = 0;
            for (int cityIndex = 0; cityIndex + 1 < this.route.Length; cityIndex++)
                totalDistance += this.route[cityIndex].distanceFrom(this.route[cityIndex + 1]);

            totalDistance += this.route[this.route.Length - 1].distanceFrom(this.route[0]);
            this.distance = totalDistance;

            return totalDistance;
        }
    }
}
