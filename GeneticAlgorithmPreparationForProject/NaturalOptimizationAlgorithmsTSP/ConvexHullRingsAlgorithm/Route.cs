﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.ConvexHullRingsAlgorithm
{
    class Route
    {
        public List<City> route;
        private double distance = 0;

        public Route(List<City> cities)
        {
            this.route = cities;
        }

        public double getDistance()
        {
            if (this.distance > 0)
                return this.distance;

            double totalDistance = 0;
            for (int cityIndex = 0; cityIndex + 1 < this.route.Count; cityIndex++)
                totalDistance += this.route[cityIndex].distanceFrom(this.route[cityIndex + 1]);

            totalDistance += this.route[this.route.Count - 1].distanceFrom(this.route[0]);
            this.distance = totalDistance;

            return totalDistance;
        }
    }
}
