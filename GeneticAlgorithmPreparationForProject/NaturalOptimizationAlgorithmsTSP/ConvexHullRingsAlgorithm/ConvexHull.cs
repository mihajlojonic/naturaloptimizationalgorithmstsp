﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.ConvexHullRingsAlgorithm
{
    class ConvexHull
    {
        public static List<City> MakeHull(List<City> cities)
        {
            List<City> newCities = new List<City>(cities);
            newCities.Sort();
            return MakeHullPresorted(newCities);
        }

        private static List<City> MakeHullPresorted(List<City> cities)
        {
            if (cities.Count <= 1)
                return new List<City>(cities);

            List<City> upperHull = new List<City>();
            foreach (City p in cities)
            {
                while (upperHull.Count >= 2)
                {
                    City q = upperHull[upperHull.Count - 1];
                    City r = upperHull[upperHull.Count - 2];
                    if ((q.X - r.X) * (p.Y - r.Y) >= (q.Y - r.Y) * (p.X - r.X))
                        upperHull.RemoveAt(upperHull.Count - 1);
                    else
                        break;
                }
                upperHull.Add(p);
            }
            upperHull.RemoveAt(upperHull.Count - 1);

            List<City> lowerHull = new List<City>();
            for (int i = cities.Count - 1; i >= 0; i--)
            {
                City p = cities[i];
                while (lowerHull.Count >= 2)
                {
                    City q = lowerHull[lowerHull.Count - 1];
                    City r = lowerHull[lowerHull.Count - 2];
                    if ((q.X - r.X) * (p.Y - r.Y) >= (q.Y - r.Y) * (p.X - r.X))
                        lowerHull.RemoveAt(lowerHull.Count - 1);
                    else
                        break;
                }
                lowerHull.Add(p);
            }
            lowerHull.RemoveAt(lowerHull.Count - 1);

            if (!(upperHull.Count == 1 && Enumerable.SequenceEqual(upperHull, lowerHull)))
                upperHull.AddRange(lowerHull);
            return upperHull;
        }
    }
}
