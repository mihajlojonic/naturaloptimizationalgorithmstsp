﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NaturalOptimizationAlgorithmsTSP.ConvexHullRingsAlgorithm
{
    class HullRings
    {
        private City[] cities;
        private List<List<City>> rings;
        private int numberOfRings;

        public List<List<City>> Rings { get => rings; set => rings = value; }
        public City[] Cities { get => cities; set => cities = value; }

        public HullRings(City[] cities, List<List<City>> rings)
        {
            this.Cities = cities;
            this.Rings = rings;
            this.numberOfRings = rings.Count;
        }

        public List<City> Solve()
        {
            List<City> returnList = new List<City>();
            City firstRingCity = this.rings.ElementAt(0).ElementAt(0);
            returnList.Add(firstRingCity);
            this.rings.ElementAt(0).RemoveAt(0);

            while(returnList.Count < this.Cities.Length)
            {
                List<City> inPath = PathDirectionIn(returnList.Last());
                returnList.AddRange(inPath.Skip(1));
//                returnList.AddRange(inPath);
                RemoveFormRings(inPath);

                List<City> outPath = PathDirectionOut(returnList.Last());
                returnList.AddRange(outPath.Skip(1));
//                returnList.AddRange(outPath);
                RemoveFormRings(outPath);
            }

            return returnList;
        }

        private List<City> PathDirectionIn(City firstRingCity)
        {
            List<City> returnList = new List<City>();
            returnList.Add(firstRingCity);
            City nearestInNextRing = new City();
            for(int ringIndex = 0; ringIndex < this.rings.Count; ringIndex++)
            {
                nearestInNextRing = FindNearest(nearestInNextRing, ringIndex);
                if (nearestInNextRing == null)
                    return returnList;
                else
                    returnList.Add(nearestInNextRing);
            }

            return returnList;
        }

        private List<City> PathDirectionOut(City lastRingCity)
        {
            List<City> returnList = new List<City>();
            returnList.Add(lastRingCity);
            City nearestInPrevRing = new City();
            for(int ringIndex = this.rings.Count - 1; ringIndex >= 0; ringIndex--)
            {
                nearestInPrevRing = FindNearest(nearestInPrevRing, ringIndex);
                if (nearestInPrevRing == null)
                    return returnList;
                else
                    returnList.Add(nearestInPrevRing);
            }

            return returnList;
        }

        private City FindNearest(City nearestInNextRing, int ringIndex)
        {
            List<City> currentRing = Rings.ElementAt(ringIndex);
            double minDistance = double.MaxValue;
            City nearestCity = null;
            for(int cityIndex = 0; cityIndex < currentRing.Count; cityIndex++)
            {
                double distance = nearestInNextRing.distanceFrom(currentRing.ElementAt(cityIndex));
                if(minDistance > distance)
                {
                    minDistance = distance;
                    nearestCity = currentRing.ElementAt(cityIndex);
                }
            }

            return nearestCity;
        }

        private void RemoveFormRings(List<City> inPath)
        {
            foreach(City city in inPath)
            {
                foreach(List<City> ring in this.rings)
                {
                    if (ring.Contains(city))
                    {
                        ring.Remove(city);
                        break;
                    }
                }
            }
        }
    }
}
