﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Accord;
using Accord.Controls;
using NaturalOptimizationAlgorithmsTSP.BeeColonyAlgorithm;
using NaturalOptimizationAlgorithmsTSP.NearestNeighbourAlgorithm;

namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    public partial class BeesUC : UserControl
    {
        private volatile bool needToStop = false;

        int numCities = 5;
        private double[,] map = null;

        public int countOfSpecialInitialize = 0;

        public int totalNumberBees = 100;
        public int numberInactive = 10;
        public int numberActive = 75;
        public int numberScout = 15;

        public int maxNumberCycles = 250;

        public int maxNumberVisits = 100;
        public double probPersuasion = 0.95;
        public double probMistake = 0.01;

        private Thread workerThread = null;
        private string consoleOutput = "";

        public BeesUC()
        {
            InitializeComponent();

            countryMap.RangeX = new Range(0, 1000);
            countryMap.RangeY = new Range(0, 1000);
            countryMap.AddDataSeries("map", Color.Red, Chart.SeriesType.Dots, 5, false);
            countryMap.AddDataSeries("path", Color.Blue, Chart.SeriesType.Line, 1, false);
        }

        private void BeesUC_Load(object sender, EventArgs e)
        {
            setDefaultValue();
            generateMap();
        }

        public void generateMap()
        {
            Random rnd = new Random();
            map = new double[numCities, 2];

            for (int i = 0; i < numCities; i++)
            {
                map[i, 0] = rnd.Next(1001);
                map[i, 1] = rnd.Next(1001);
            }

            countryMap.UpdateDataSeries("map", map);
            countryMap.UpdateDataSeries("path", null);
        }

        public void setDefaultValue()
        {
            this.numCities = Convert.ToInt32(this.numericCities.Value);
        }

        private void UpdateSettings()
        {
            consoleOutput = "";
            chartAnts.Series[0].Points.Clear();

            if(Convert.ToDouble((this.numericActiveBees.Value + this.numericInactiveBees.Value + this.numericScoutBees.Value)) < 1.00)
            {
                this.numericActiveBees.Value += 1.00m - (this.numericActiveBees.Value + this.numericInactiveBees.Value + this.numericScoutBees.Value);
            }
            else if(Convert.ToDouble((this.numericActiveBees.Value + this.numericInactiveBees.Value + this.numericScoutBees.Value)) > 1.00)
            {
                this.numericActiveBees.Value -= 1.00m - (this.numericActiveBees.Value + this.numericInactiveBees.Value + this.numericScoutBees.Value);
            }

            this.totalNumberBees = Convert.ToInt32(this.numericColonySize.Value);
            this.numberActive = Convert.ToInt32(this.numericActiveBees.Value * this.totalNumberBees);
            this.numberInactive = Convert.ToInt32(this.numericInactiveBees.Value * this.totalNumberBees);
            this.numberScout = Convert.ToInt32(this.numericScoutBees.Value * this.totalNumberBees);
            this.probMistake = Convert.ToDouble(this.numericProbMistake.Value);
            this.probPersuasion = Convert.ToDouble(this.numericProbPersuasion.Value);
            this.maxNumberVisits = Convert.ToInt32(this.numericMaxNumberOfVisits.Value);
            this.maxNumberCycles = Convert.ToInt32(this.numericMaxNumberOfRounds.Value);
        }

        private string getSpecialInitialize()
        {
            return comboInitialize.Text;
        }

        private void SearchSolution()
        {
            richConsole.Text = "";
            UpdateSettings();
            City[] cities = new City[numCities];
            Random rnd = new Random();

            for (int cityIndex = 0; cityIndex < numCities; cityIndex++)
            {
                double xPos = map[cityIndex, 0];
                double yPos = map[cityIndex, 1];
                cities[cityIndex] = new City(xPos, yPos);
            }

            if(getSpecialInitialize() == "Nearest Neighbour")
            {
                NNAlgorithm nna = new NNAlgorithm(cities);
                nna.Slove();
                cities = nna.Cities;
                countOfSpecialInitialize = Convert.ToInt32(numCities * this.numericSpecialInitialize.Value);
            }

            Hive hive = new Hive(totalNumberBees, numberInactive, numberActive, numberScout, maxNumberVisits, maxNumberCycles, probPersuasion, probMistake, cities, countOfSpecialInitialize);

            int cycle = 0;

            while (cycle <= this.maxNumberCycles && !needToStop)
            {
                for (int i = 0; i < this.totalNumberBees; ++i) 
                {
                    switch (hive.bees[i].status)
                    {
                        case BeeStatus.active:
                            hive.ProcessActiveBee(i);
                            break;
                        case BeeStatus.scout:
                            hive.ProcessScoutBee(i);
                            break;
                        case BeeStatus.inactive:
                            hive.ProcessInactiveBee(i);
                            break;
                    }
                }
                BeeColonyAlgorithm.Route route = new BeeColonyAlgorithm.Route(hive.bestBee, cities);
                consoleOutput = consoleOutput + Environment.NewLine + "Round: " + cycle + ", Best distance: " + route.getDistance();
                upDateChart(cycle, route.getDistance());
                richConsole.Text = consoleOutput;
                lblCurrentTimeValue.Text = cycle.ToString();
                lblCurrentTimeBestDistance.Text = route.getDistance().ToString();
                upDateMap(route);

                ++cycle;
            } 
        }

        private void upDateChart(int time, double v)
        {
            chartAnts.ChartAreas[0].CursorX.IsUserEnabled = true;
            chartAnts.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

            chartAnts.Series[0].Points.AddXY(time, v);
        }

        private void upDateMap(NaturalOptimizationAlgorithmsTSP.BeeColonyAlgorithm.Route route)
        {
            double[,] path = new double[numCities + 1, 2];
            City[] bestRoute = route.route;
            for (int i = 0; i < bestRoute.Length; i++)
            {
                path[i, 0] = bestRoute[i].X;
                path[i, 1] = bestRoute[i].Y;
            }

            path[bestRoute.Length, 0] = bestRoute[0].X;
            path[bestRoute.Length, 1] = bestRoute[0].Y;

            countryMap.UpdateDataSeries("path", path);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            UpdateSettings();
            needToStop = false;
            workerThread = new Thread(new ThreadStart(SearchSolution));
            workerThread.Start();
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            this.numCities = Convert.ToInt32(this.numericCities.Value);
            generateMap();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (workerThread != null)
            {
                needToStop = true;
                while (!workerThread.Join(100))
                    Application.DoEvents();
                workerThread = null;
            }
        }
    }
}
