﻿namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    partial class GeneticUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.btnStop = new System.Windows.Forms.Button();
            this.groupCurrentGeneration = new System.Windows.Forms.GroupBox();
            this.lblCurrentGenerationBestDistance = new System.Windows.Forms.Label();
            this.lblCurrentGenerationValue = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupInitialization = new System.Windows.Forms.GroupBox();
            this.radioRandom = new System.Windows.Forms.RadioButton();
            this.numericMassPoint = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.radioMassPoint = new System.Windows.Forms.RadioButton();
            this.numericGreedy = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.radioGreedy = new System.Windows.Forms.RadioButton();
            this.radioNearestNeighbour = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.numericNNA = new System.Windows.Forms.NumericUpDown();
            this.numericPopulationSize = new System.Windows.Forms.NumericUpDown();
            this.numericElitismRate = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericGenerationsNumber = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.lblTurnamentSize = new System.Windows.Forms.Label();
            this.numericTurnamentSize = new System.Windows.Forms.NumericUpDown();
            this.numericCrossoverRate = new System.Windows.Forms.NumericUpDown();
            this.numericMutationRate = new System.Windows.Forms.NumericUpDown();
            this.lblCrossoverRate = new System.Windows.Forms.Label();
            this.lblMutationRate = new System.Windows.Forms.Label();
            this.comboCrossover = new System.Windows.Forms.ComboBox();
            this.lblCrossover = new System.Windows.Forms.Label();
            this.comboMutation = new System.Windows.Forms.ComboBox();
            this.lblMutation = new System.Windows.Forms.Label();
            this.comboSelection = new System.Windows.Forms.ComboBox();
            this.lblSelection = new System.Windows.Forms.Label();
            this.lblPopulationSize = new System.Windows.Forms.Label();
            this.tabConsole = new System.Windows.Forms.TabPage();
            this.richConsole = new System.Windows.Forms.RichTextBox();
            this.tabLiveChart = new System.Windows.Forms.TabPage();
            this.chartGenetic = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.gbMapCountry = new System.Windows.Forms.GroupBox();
            this.numericCitiesCount = new System.Windows.Forms.NumericUpDown();
            this.btnGenerateCitiesCount = new System.Windows.Forms.Button();
            this.countryMap = new Accord.Controls.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.groupCurrentGeneration.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupInitialization.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMassPoint)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGreedy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericNNA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPopulationSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericElitismRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGenerationsNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTurnamentSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCrossoverRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMutationRate)).BeginInit();
            this.tabConsole.SuspendLayout();
            this.tabLiveChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartGenetic)).BeginInit();
            this.gbMapCountry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCitiesCount)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(1013, 458);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(124, 36);
            this.btnStop.TabIndex = 23;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // groupCurrentGeneration
            // 
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentGenerationBestDistance);
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentGenerationValue);
            this.groupCurrentGeneration.Controls.Add(this.label4);
            this.groupCurrentGeneration.Controls.Add(this.label3);
            this.groupCurrentGeneration.Location = new System.Drawing.Point(567, 386);
            this.groupCurrentGeneration.Name = "groupCurrentGeneration";
            this.groupCurrentGeneration.Size = new System.Drawing.Size(570, 66);
            this.groupCurrentGeneration.TabIndex = 24;
            this.groupCurrentGeneration.TabStop = false;
            this.groupCurrentGeneration.Text = "Current generation";
            // 
            // lblCurrentGenerationBestDistance
            // 
            this.lblCurrentGenerationBestDistance.AutoSize = true;
            this.lblCurrentGenerationBestDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentGenerationBestDistance.ForeColor = System.Drawing.Color.Blue;
            this.lblCurrentGenerationBestDistance.Location = new System.Drawing.Point(381, 18);
            this.lblCurrentGenerationBestDistance.Name = "lblCurrentGenerationBestDistance";
            this.lblCurrentGenerationBestDistance.Size = new System.Drawing.Size(84, 39);
            this.lblCurrentGenerationBestDistance.TabIndex = 3;
            this.lblCurrentGenerationBestDistance.Text = "0.00";
            // 
            // lblCurrentGenerationValue
            // 
            this.lblCurrentGenerationValue.AutoSize = true;
            this.lblCurrentGenerationValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentGenerationValue.ForeColor = System.Drawing.Color.Red;
            this.lblCurrentGenerationValue.Location = new System.Drawing.Point(138, 18);
            this.lblCurrentGenerationValue.Name = "lblCurrentGenerationValue";
            this.lblCurrentGenerationValue.Size = new System.Drawing.Size(36, 39);
            this.lblCurrentGenerationValue.TabIndex = 2;
            this.lblCurrentGenerationValue.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F);
            this.label4.Location = new System.Drawing.Point(226, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 26);
            this.label4.TabIndex = 1;
            this.label4.Text = "Best distance:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F);
            this.label3.Location = new System.Drawing.Point(7, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "Generation:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(848, 458);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(131, 36);
            this.btnStart.TabIndex = 22;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.tabPage1);
            this.tabSettings.Controls.Add(this.tabConsole);
            this.tabSettings.Controls.Add(this.tabLiveChart);
            this.tabSettings.Location = new System.Drawing.Point(567, 15);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(574, 365);
            this.tabSettings.TabIndex = 21;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupInitialization);
            this.tabPage1.Controls.Add(this.numericPopulationSize);
            this.tabPage1.Controls.Add(this.numericElitismRate);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.numericGenerationsNumber);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.lblTurnamentSize);
            this.tabPage1.Controls.Add(this.numericTurnamentSize);
            this.tabPage1.Controls.Add(this.numericCrossoverRate);
            this.tabPage1.Controls.Add(this.numericMutationRate);
            this.tabPage1.Controls.Add(this.lblCrossoverRate);
            this.tabPage1.Controls.Add(this.lblMutationRate);
            this.tabPage1.Controls.Add(this.comboCrossover);
            this.tabPage1.Controls.Add(this.lblCrossover);
            this.tabPage1.Controls.Add(this.comboMutation);
            this.tabPage1.Controls.Add(this.lblMutation);
            this.tabPage1.Controls.Add(this.comboSelection);
            this.tabPage1.Controls.Add(this.lblSelection);
            this.tabPage1.Controls.Add(this.lblPopulationSize);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(566, 336);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "TravelingSalesman";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupInitialization
            // 
            this.groupInitialization.Controls.Add(this.radioRandom);
            this.groupInitialization.Controls.Add(this.numericMassPoint);
            this.groupInitialization.Controls.Add(this.label8);
            this.groupInitialization.Controls.Add(this.radioMassPoint);
            this.groupInitialization.Controls.Add(this.numericGreedy);
            this.groupInitialization.Controls.Add(this.label6);
            this.groupInitialization.Controls.Add(this.radioGreedy);
            this.groupInitialization.Controls.Add(this.radioNearestNeighbour);
            this.groupInitialization.Controls.Add(this.label5);
            this.groupInitialization.Controls.Add(this.numericNNA);
            this.groupInitialization.Location = new System.Drawing.Point(10, 165);
            this.groupInitialization.Name = "groupInitialization";
            this.groupInitialization.Size = new System.Drawing.Size(550, 145);
            this.groupInitialization.TabIndex = 4;
            this.groupInitialization.TabStop = false;
            this.groupInitialization.Text = "Initialization";
            // 
            // radioRandom
            // 
            this.radioRandom.Checked = true;
            this.radioRandom.Location = new System.Drawing.Point(0, 112);
            this.radioRandom.Name = "radioRandom";
            this.radioRandom.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioRandom.Size = new System.Drawing.Size(350, 24);
            this.radioRandom.TabIndex = 34;
            this.radioRandom.TabStop = true;
            this.radioRandom.Text = "Random";
            this.radioRandom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioRandom.UseVisualStyleBackColor = true;
            // 
            // numericMassPoint
            // 
            this.numericMassPoint.DecimalPlaces = 2;
            this.numericMassPoint.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericMassPoint.Location = new System.Drawing.Point(472, 84);
            this.numericMassPoint.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMassPoint.Name = "numericMassPoint";
            this.numericMassPoint.Size = new System.Drawing.Size(57, 22);
            this.numericMassPoint.TabIndex = 33;
            this.numericMassPoint.Value = new decimal(new int[] {
            10,
            0,
            0,
            131072});
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(359, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(74, 17);
            this.label8.TabIndex = 32;
            this.label8.Text = "Mass rate:";
            // 
            // radioMassPoint
            // 
            this.radioMassPoint.Location = new System.Drawing.Point(0, 82);
            this.radioMassPoint.Name = "radioMassPoint";
            this.radioMassPoint.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioMassPoint.Size = new System.Drawing.Size(350, 24);
            this.radioMassPoint.TabIndex = 31;
            this.radioMassPoint.Text = "Initialize by Mass Point";
            this.radioMassPoint.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioMassPoint.UseVisualStyleBackColor = true;
            // 
            // numericGreedy
            // 
            this.numericGreedy.DecimalPlaces = 2;
            this.numericGreedy.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericGreedy.Location = new System.Drawing.Point(472, 51);
            this.numericGreedy.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericGreedy.Name = "numericGreedy";
            this.numericGreedy.Size = new System.Drawing.Size(57, 22);
            this.numericGreedy.TabIndex = 27;
            this.numericGreedy.Value = new decimal(new int[] {
            10,
            0,
            0,
            131072});
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(359, 55);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 17);
            this.label6.TabIndex = 28;
            this.label6.Text = "Greedy rate:";
            // 
            // radioGreedy
            // 
            this.radioGreedy.Location = new System.Drawing.Point(0, 51);
            this.radioGreedy.Name = "radioGreedy";
            this.radioGreedy.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioGreedy.Size = new System.Drawing.Size(350, 24);
            this.radioGreedy.TabIndex = 30;
            this.radioGreedy.Text = "Initialize with Greedy Algorithm";
            this.radioGreedy.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioGreedy.UseVisualStyleBackColor = true;
            // 
            // radioNearestNeighbour
            // 
            this.radioNearestNeighbour.Location = new System.Drawing.Point(0, 21);
            this.radioNearestNeighbour.Name = "radioNearestNeighbour";
            this.radioNearestNeighbour.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.radioNearestNeighbour.Size = new System.Drawing.Size(350, 24);
            this.radioNearestNeighbour.TabIndex = 29;
            this.radioNearestNeighbour.Text = "Initialize with Nearest Neighbour Algorithm";
            this.radioNearestNeighbour.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.radioNearestNeighbour.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(359, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 17);
            this.label5.TabIndex = 25;
            this.label5.Text = "NNA rate:";
            // 
            // numericNNA
            // 
            this.numericNNA.DecimalPlaces = 2;
            this.numericNNA.Location = new System.Drawing.Point(472, 23);
            this.numericNNA.Maximum = new decimal(new int[] {
            100,
            0,
            0,
            131072});
            this.numericNNA.Name = "numericNNA";
            this.numericNNA.Size = new System.Drawing.Size(57, 22);
            this.numericNNA.TabIndex = 24;
            this.numericNNA.Value = new decimal(new int[] {
            10,
            0,
            0,
            131072});
            // 
            // numericPopulationSize
            // 
            this.numericPopulationSize.Location = new System.Drawing.Point(132, 7);
            this.numericPopulationSize.Name = "numericPopulationSize";
            this.numericPopulationSize.Size = new System.Drawing.Size(225, 22);
            this.numericPopulationSize.TabIndex = 22;
            this.numericPopulationSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericPopulationSize.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numericElitismRate
            // 
            this.numericElitismRate.DecimalPlaces = 2;
            this.numericElitismRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericElitismRate.Location = new System.Drawing.Point(482, 11);
            this.numericElitismRate.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericElitismRate.Name = "numericElitismRate";
            this.numericElitismRate.Size = new System.Drawing.Size(57, 22);
            this.numericElitismRate.TabIndex = 21;
            this.numericElitismRate.Value = new decimal(new int[] {
            20,
            0,
            0,
            131072});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(367, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Elitism rate:";
            // 
            // numericGenerationsNumber
            // 
            this.numericGenerationsNumber.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericGenerationsNumber.Location = new System.Drawing.Point(132, 311);
            this.numericGenerationsNumber.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericGenerationsNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericGenerationsNumber.Name = "numericGenerationsNumber";
            this.numericGenerationsNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericGenerationsNumber.Size = new System.Drawing.Size(225, 22);
            this.numericGenerationsNumber.TabIndex = 18;
            this.numericGenerationsNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericGenerationsNumber.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 313);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Generations num:";
            // 
            // lblTurnamentSize
            // 
            this.lblTurnamentSize.AutoSize = true;
            this.lblTurnamentSize.Enabled = false;
            this.lblTurnamentSize.Location = new System.Drawing.Point(364, 50);
            this.lblTurnamentSize.Name = "lblTurnamentSize";
            this.lblTurnamentSize.Size = new System.Drawing.Size(110, 17);
            this.lblTurnamentSize.TabIndex = 16;
            this.lblTurnamentSize.Text = "Turnament size:";
            // 
            // numericTurnamentSize
            // 
            this.numericTurnamentSize.DecimalPlaces = 2;
            this.numericTurnamentSize.Enabled = false;
            this.numericTurnamentSize.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericTurnamentSize.Location = new System.Drawing.Point(482, 48);
            this.numericTurnamentSize.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericTurnamentSize.Name = "numericTurnamentSize";
            this.numericTurnamentSize.Size = new System.Drawing.Size(57, 22);
            this.numericTurnamentSize.TabIndex = 15;
            this.numericTurnamentSize.Value = new decimal(new int[] {
            10,
            0,
            0,
            131072});
            // 
            // numericCrossoverRate
            // 
            this.numericCrossoverRate.DecimalPlaces = 2;
            this.numericCrossoverRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericCrossoverRate.Location = new System.Drawing.Point(482, 129);
            this.numericCrossoverRate.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericCrossoverRate.Name = "numericCrossoverRate";
            this.numericCrossoverRate.Size = new System.Drawing.Size(57, 22);
            this.numericCrossoverRate.TabIndex = 12;
            this.numericCrossoverRate.Value = new decimal(new int[] {
            75,
            0,
            0,
            131072});
            // 
            // numericMutationRate
            // 
            this.numericMutationRate.DecimalPlaces = 2;
            this.numericMutationRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericMutationRate.Location = new System.Drawing.Point(482, 88);
            this.numericMutationRate.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMutationRate.Name = "numericMutationRate";
            this.numericMutationRate.Size = new System.Drawing.Size(57, 22);
            this.numericMutationRate.TabIndex = 11;
            this.numericMutationRate.Value = new decimal(new int[] {
            25,
            0,
            0,
            131072});
            // 
            // lblCrossoverRate
            // 
            this.lblCrossoverRate.AutoSize = true;
            this.lblCrossoverRate.Location = new System.Drawing.Point(364, 130);
            this.lblCrossoverRate.Name = "lblCrossoverRate";
            this.lblCrossoverRate.Size = new System.Drawing.Size(105, 17);
            this.lblCrossoverRate.TabIndex = 10;
            this.lblCrossoverRate.Text = "Crossover rate:";
            // 
            // lblMutationRate
            // 
            this.lblMutationRate.AutoSize = true;
            this.lblMutationRate.Location = new System.Drawing.Point(366, 89);
            this.lblMutationRate.Name = "lblMutationRate";
            this.lblMutationRate.Size = new System.Drawing.Size(95, 17);
            this.lblMutationRate.TabIndex = 8;
            this.lblMutationRate.Text = "Mutation rate:";
            // 
            // comboCrossover
            // 
            this.comboCrossover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboCrossover.FormattingEnabled = true;
            this.comboCrossover.Items.AddRange(new object[] {
            "Ordered crossover",
            "SinglePoint crossover",
            "Random crossover"});
            this.comboCrossover.Location = new System.Drawing.Point(132, 127);
            this.comboCrossover.Name = "comboCrossover";
            this.comboCrossover.Size = new System.Drawing.Size(225, 24);
            this.comboCrossover.TabIndex = 7;
            // 
            // lblCrossover
            // 
            this.lblCrossover.AutoSize = true;
            this.lblCrossover.Location = new System.Drawing.Point(10, 130);
            this.lblCrossover.Name = "lblCrossover";
            this.lblCrossover.Size = new System.Drawing.Size(76, 17);
            this.lblCrossover.TabIndex = 6;
            this.lblCrossover.Text = "Crossover:";
            // 
            // comboMutation
            // 
            this.comboMutation.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboMutation.FormattingEnabled = true;
            this.comboMutation.Items.AddRange(new object[] {
            "Swap mutation",
            "Uniform mutation",
            "Scramble mutation",
            "Inversion mutation"});
            this.comboMutation.Location = new System.Drawing.Point(132, 86);
            this.comboMutation.Name = "comboMutation";
            this.comboMutation.Size = new System.Drawing.Size(225, 24);
            this.comboMutation.TabIndex = 5;
            // 
            // lblMutation
            // 
            this.lblMutation.AutoSize = true;
            this.lblMutation.Location = new System.Drawing.Point(10, 89);
            this.lblMutation.Name = "lblMutation";
            this.lblMutation.Size = new System.Drawing.Size(66, 17);
            this.lblMutation.TabIndex = 4;
            this.lblMutation.Text = "Mutation:";
            // 
            // comboSelection
            // 
            this.comboSelection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboSelection.FormattingEnabled = true;
            this.comboSelection.Items.AddRange(new object[] {
            "Roulete wheel slection",
            "Turnament selection",
            "Rank selection",
            "Elite selection"});
            this.comboSelection.Location = new System.Drawing.Point(132, 44);
            this.comboSelection.Name = "comboSelection";
            this.comboSelection.Size = new System.Drawing.Size(225, 24);
            this.comboSelection.TabIndex = 3;
            this.comboSelection.SelectedIndexChanged += new System.EventHandler(this.comboSelection_SelectedIndexChanged);
            // 
            // lblSelection
            // 
            this.lblSelection.AutoSize = true;
            this.lblSelection.Location = new System.Drawing.Point(10, 44);
            this.lblSelection.Name = "lblSelection";
            this.lblSelection.Size = new System.Drawing.Size(70, 17);
            this.lblSelection.TabIndex = 2;
            this.lblSelection.Text = "Selection:";
            // 
            // lblPopulationSize
            // 
            this.lblPopulationSize.AutoSize = true;
            this.lblPopulationSize.Location = new System.Drawing.Point(10, 7);
            this.lblPopulationSize.Name = "lblPopulationSize";
            this.lblPopulationSize.Size = new System.Drawing.Size(108, 17);
            this.lblPopulationSize.TabIndex = 0;
            this.lblPopulationSize.Text = "Population size:";
            // 
            // tabConsole
            // 
            this.tabConsole.Controls.Add(this.richConsole);
            this.tabConsole.Location = new System.Drawing.Point(4, 25);
            this.tabConsole.Name = "tabConsole";
            this.tabConsole.Padding = new System.Windows.Forms.Padding(3);
            this.tabConsole.Size = new System.Drawing.Size(566, 336);
            this.tabConsole.TabIndex = 1;
            this.tabConsole.Text = "Console";
            this.tabConsole.UseVisualStyleBackColor = true;
            // 
            // richConsole
            // 
            this.richConsole.Location = new System.Drawing.Point(8, 7);
            this.richConsole.Name = "richConsole";
            this.richConsole.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richConsole.Size = new System.Drawing.Size(552, 302);
            this.richConsole.TabIndex = 0;
            this.richConsole.Text = "";
            // 
            // tabLiveChart
            // 
            this.tabLiveChart.Controls.Add(this.chartGenetic);
            this.tabLiveChart.Location = new System.Drawing.Point(4, 25);
            this.tabLiveChart.Name = "tabLiveChart";
            this.tabLiveChart.Padding = new System.Windows.Forms.Padding(3);
            this.tabLiveChart.Size = new System.Drawing.Size(566, 336);
            this.tabLiveChart.TabIndex = 2;
            this.tabLiveChart.Text = "Chart";
            this.tabLiveChart.UseVisualStyleBackColor = true;
            // 
            // chartGenetic
            // 
            chartArea1.Name = "ChartArea1";
            this.chartGenetic.ChartAreas.Add(chartArea1);
            this.chartGenetic.Location = new System.Drawing.Point(8, 16);
            this.chartGenetic.Name = "chartGenetic";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series1.Name = "Series1";
            this.chartGenetic.Series.Add(series1);
            this.chartGenetic.Size = new System.Drawing.Size(552, 300);
            this.chartGenetic.TabIndex = 0;
            this.chartGenetic.Text = "Chart - Genetic algorithm";
            // 
            // gbMapCountry
            // 
            this.gbMapCountry.Controls.Add(this.numericCitiesCount);
            this.gbMapCountry.Controls.Add(this.btnGenerateCitiesCount);
            this.gbMapCountry.Controls.Add(this.countryMap);
            this.gbMapCountry.Controls.Add(this.label1);
            this.gbMapCountry.Location = new System.Drawing.Point(6, 15);
            this.gbMapCountry.Name = "gbMapCountry";
            this.gbMapCountry.Size = new System.Drawing.Size(555, 485);
            this.gbMapCountry.TabIndex = 25;
            this.gbMapCountry.TabStop = false;
            this.gbMapCountry.Text = "Country map";
            // 
            // numericCitiesCount
            // 
            this.numericCitiesCount.Location = new System.Drawing.Point(71, 451);
            this.numericCitiesCount.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericCitiesCount.Name = "numericCitiesCount";
            this.numericCitiesCount.Size = new System.Drawing.Size(120, 22);
            this.numericCitiesCount.TabIndex = 1;
            this.numericCitiesCount.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // btnGenerateCitiesCount
            // 
            this.btnGenerateCitiesCount.Location = new System.Drawing.Point(225, 443);
            this.btnGenerateCitiesCount.Name = "btnGenerateCitiesCount";
            this.btnGenerateCitiesCount.Size = new System.Drawing.Size(105, 36);
            this.btnGenerateCitiesCount.TabIndex = 3;
            this.btnGenerateCitiesCount.Text = "Generate";
            this.btnGenerateCitiesCount.UseVisualStyleBackColor = true;
            this.btnGenerateCitiesCount.Click += new System.EventHandler(this.btnGenerateCitiesCount_Click);
            // 
            // countryMap
            // 
            this.countryMap.Location = new System.Drawing.Point(7, 17);
            this.countryMap.Name = "countryMap";
            this.countryMap.Size = new System.Drawing.Size(542, 420);
            this.countryMap.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 453);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cities:";
            // 
            // GeneticUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.gbMapCountry);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.groupCurrentGeneration);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tabSettings);
            this.Name = "GeneticUC";
            this.Size = new System.Drawing.Size(1163, 510);
            this.Load += new System.EventHandler(this.GeneticUC_Load);
            this.groupCurrentGeneration.ResumeLayout(false);
            this.groupCurrentGeneration.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.groupInitialization.ResumeLayout(false);
            this.groupInitialization.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMassPoint)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGreedy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericNNA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericPopulationSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericElitismRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGenerationsNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericTurnamentSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCrossoverRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMutationRate)).EndInit();
            this.tabConsole.ResumeLayout(false);
            this.tabLiveChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartGenetic)).EndInit();
            this.gbMapCountry.ResumeLayout(false);
            this.gbMapCountry.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCitiesCount)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox groupCurrentGeneration;
        private System.Windows.Forms.Label lblCurrentGenerationBestDistance;
        private System.Windows.Forms.Label lblCurrentGenerationValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.NumericUpDown numericPopulationSize;
        private System.Windows.Forms.NumericUpDown numericElitismRate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericGenerationsNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblTurnamentSize;
        private System.Windows.Forms.NumericUpDown numericTurnamentSize;
        private System.Windows.Forms.NumericUpDown numericCrossoverRate;
        private System.Windows.Forms.NumericUpDown numericMutationRate;
        private System.Windows.Forms.Label lblCrossoverRate;
        private System.Windows.Forms.Label lblMutationRate;
        private System.Windows.Forms.ComboBox comboCrossover;
        private System.Windows.Forms.Label lblCrossover;
        private System.Windows.Forms.ComboBox comboMutation;
        private System.Windows.Forms.Label lblMutation;
        private System.Windows.Forms.ComboBox comboSelection;
        private System.Windows.Forms.Label lblSelection;
        private System.Windows.Forms.Label lblPopulationSize;
        private System.Windows.Forms.TabPage tabConsole;
        private System.Windows.Forms.RichTextBox richConsole;
        private System.Windows.Forms.GroupBox gbMapCountry;
        private System.Windows.Forms.NumericUpDown numericCitiesCount;
        private System.Windows.Forms.Button btnGenerateCitiesCount;
        private Accord.Controls.Chart countryMap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numericGreedy;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericNNA;
        private System.Windows.Forms.GroupBox groupInitialization;
        private System.Windows.Forms.NumericUpDown numericMassPoint;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RadioButton radioMassPoint;
        private System.Windows.Forms.RadioButton radioGreedy;
        private System.Windows.Forms.RadioButton radioNearestNeighbour;
        private System.Windows.Forms.RadioButton radioRandom;
        private System.Windows.Forms.TabPage tabLiveChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartGenetic;
    }
}
