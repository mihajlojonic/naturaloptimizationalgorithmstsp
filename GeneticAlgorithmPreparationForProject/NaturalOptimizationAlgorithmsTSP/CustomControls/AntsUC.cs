﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.Controls;
using Accord;
using System.Threading;
using NaturalOptimizationAlgorithmsTSP.AntColonyAlgorithm;

namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    public partial class AntsUC : UserControl
    {
        private volatile bool needToStop = false;

        int numCities = 5;
        private double[,] map = null;

        private int Alpha = 3;
        private int Beta = 2;
        private double RHO = 0.01;
        private double Q = 2.0;
        private int maxTime = 100;
        private int antsCount = 100;
        private int elismCount = 1;

        private Thread workerThread = null;
        private string consoleOutput = "";

        public AntsUC()
        {
            InitializeComponent();
            
            countryMap.RangeX = new Range(0, 1000);
            countryMap.RangeY = new Range(0, 1000);
            countryMap.AddDataSeries("map", Color.Red, Chart.SeriesType.Dots, 5, false);
            countryMap.AddDataSeries("path", Color.Blue, Chart.SeriesType.Line, 1, false);
        }

        private void AntsUC_Load(object sender, EventArgs e)
        {
            setDefaultValue();
            generateMap();
        }

        public void generateMap()
        {
            Random rnd = new Random();
            map = new double[numCities, 2];

            for (int i = 0; i < numCities; i++)
            {
                map[i, 0] = rnd.Next(1001);
                map[i, 1] = rnd.Next(1001);
            }

            countryMap.UpdateDataSeries("map", map);
            countryMap.UpdateDataSeries("path", null);
        }

        public void setDefaultValue()
        {
            this.numCities = Convert.ToInt32(this.numericCities.Value);
        }

        private void UpdateSettings()
        {
            consoleOutput = "";
            chartAnts.Series[0].Points.Clear();

            this.Alpha = Convert.ToInt32(this.numericAlpha.Value);
            this.Beta = Convert.ToInt32(this.numericBeta.Value);
            this.RHO = Convert.ToDouble(this.numericRHO.Value);
            this.Q = Convert.ToDouble(this.numericQ.Value);
            this.antsCount = Convert.ToInt32(this.numericColonySize.Value);
            this.maxTime = Convert.ToInt32(this.numericGenerationsNumber.Value);
            this.elismCount = Convert.ToInt32(this.antsCount * numericElitismProcent.Value);
        }

        private void SearchSolution()
        {
            richConsole.Text = "";
            UpdateSettings();
            City[] cities = new City[numCities];
            Random rnd = new Random();

            for (int cityIndex = 0; cityIndex < numCities; cityIndex++)
            {
                double xPos = map[cityIndex, 0];
                double yPos = map[cityIndex, 1];
                cities[cityIndex] = new City(xPos, yPos);
            }


            ACAlgtorithm aca = new ACAlgtorithm(numCities, this.antsCount, this.maxTime, this.elismCount);

            aca.MakeGraphDistances(cities);
            aca.InitColony();
            aca.evalColony(cities);
            AntColonyAlgorithm.Route route = new AntColonyAlgorithm.Route(aca.Colony.GetFittest(0), cities);
            upDateMap(route);

            aca.initPheromones();

            int time = 0;
            while (time <= aca.MaxTime && !needToStop)
            {
                aca.updateColony();
                aca.updatePheromones();
                aca.evalColony(cities);
                AntColonyAlgorithm.Route routeLoop = new AntColonyAlgorithm.Route(aca.Colony.GetFittest(0), cities);
                consoleOutput = consoleOutput + Environment.NewLine + "Round: " + time + ", Best distance: " + routeLoop.getDistance();
                upDateChart(time, routeLoop.getDistance());
                richConsole.Text = consoleOutput;
                lblCurrentTimeValue.Text = time.ToString();
                lblCurrentTimeBestDistance.Text = routeLoop.getDistance().ToString();
                upDateMap(routeLoop);
                time++;
            }

        }

        private void upDateChart(int time, double v)
        {
            chartAnts.ChartAreas[0].CursorX.IsUserEnabled = true;
            chartAnts.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;

            chartAnts.Series[0].Points.AddXY(time, v);
        }

        private void upDateMap(NaturalOptimizationAlgorithmsTSP.AntColonyAlgorithm.Route route)
        {
            double[,] path = new double[numCities + 1, 2];
            City[] bestRoute = route.route;
            for (int i = 0; i < bestRoute.Length; i++)
            {
                path[i, 0] = bestRoute[i].X;
                path[i, 1] = bestRoute[i].Y;
            }

            path[bestRoute.Length, 0] = bestRoute[0].X;
            path[bestRoute.Length, 1] = bestRoute[0].Y;

            countryMap.UpdateDataSeries("path", path);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            this.numCities = Convert.ToInt32(this.numericCities.Value);
            generateMap();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            UpdateSettings();
            needToStop = false;
            workerThread = new Thread(new ThreadStart(SearchSolution));
            workerThread.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (workerThread != null)
            {
                needToStop = true;
                while (!workerThread.Join(100))
                    Application.DoEvents();
                workerThread = null;
            }
        }
    }
}
