﻿namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    partial class AntsUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupCurrentGeneration = new System.Windows.Forms.GroupBox();
            this.lblCurrentTimeBestDistance = new System.Windows.Forms.Label();
            this.lblCurrentTimeValue = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.numericElitismProcent = new System.Windows.Forms.NumericUpDown();
            this.numericColonySize = new System.Windows.Forms.NumericUpDown();
            this.numericAlpha = new System.Windows.Forms.NumericUpDown();
            this.lblRHO = new System.Windows.Forms.Label();
            this.numericGenerationsNumber = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.lblQ = new System.Windows.Forms.Label();
            this.numericBeta = new System.Windows.Forms.NumericUpDown();
            this.numericQ = new System.Windows.Forms.NumericUpDown();
            this.numericRHO = new System.Windows.Forms.NumericUpDown();
            this.lblBeta = new System.Windows.Forms.Label();
            this.lblAlpha = new System.Windows.Forms.Label();
            this.lblPopulationSize = new System.Windows.Forms.Label();
            this.tabConsole = new System.Windows.Forms.TabPage();
            this.richConsole = new System.Windows.Forms.RichTextBox();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.numericCities = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.countryMap = new Accord.Controls.Chart();
            this.tabChart = new System.Windows.Forms.TabPage();
            this.chartAnts = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupCurrentGeneration.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericElitismProcent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericColonySize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAlpha)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGenerationsNumber)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericBeta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericQ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRHO)).BeginInit();
            this.tabConsole.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCities)).BeginInit();
            this.tabChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAnts)).BeginInit();
            this.SuspendLayout();
            // 
            // groupCurrentGeneration
            // 
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentTimeBestDistance);
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentTimeValue);
            this.groupCurrentGeneration.Controls.Add(this.label4);
            this.groupCurrentGeneration.Controls.Add(this.label3);
            this.groupCurrentGeneration.Location = new System.Drawing.Point(578, 278);
            this.groupCurrentGeneration.Name = "groupCurrentGeneration";
            this.groupCurrentGeneration.Size = new System.Drawing.Size(570, 150);
            this.groupCurrentGeneration.TabIndex = 30;
            this.groupCurrentGeneration.TabStop = false;
            this.groupCurrentGeneration.Text = "Current generation";
            // 
            // lblCurrentTimeBestDistance
            // 
            this.lblCurrentTimeBestDistance.AutoSize = true;
            this.lblCurrentTimeBestDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentTimeBestDistance.ForeColor = System.Drawing.Color.Blue;
            this.lblCurrentTimeBestDistance.Location = new System.Drawing.Point(228, 83);
            this.lblCurrentTimeBestDistance.Name = "lblCurrentTimeBestDistance";
            this.lblCurrentTimeBestDistance.Size = new System.Drawing.Size(84, 39);
            this.lblCurrentTimeBestDistance.TabIndex = 3;
            this.lblCurrentTimeBestDistance.Text = "0.00";
            // 
            // lblCurrentTimeValue
            // 
            this.lblCurrentTimeValue.AutoSize = true;
            this.lblCurrentTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentTimeValue.ForeColor = System.Drawing.Color.Red;
            this.lblCurrentTimeValue.Location = new System.Drawing.Point(221, 29);
            this.lblCurrentTimeValue.Name = "lblCurrentTimeValue";
            this.lblCurrentTimeValue.Size = new System.Drawing.Size(36, 39);
            this.lblCurrentTimeValue.TabIndex = 2;
            this.lblCurrentTimeValue.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.8F);
            this.label4.Location = new System.Drawing.Point(6, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(201, 36);
            this.label4.TabIndex = 1;
            this.label4.Text = "Best distance:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.8F);
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 36);
            this.label3.TabIndex = 0;
            this.label3.Text = "Time(rounds):";
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.tabPage1);
            this.tabSettings.Controls.Add(this.tabConsole);
            this.tabSettings.Controls.Add(this.tabChart);
            this.tabSettings.Location = new System.Drawing.Point(578, 25);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(574, 240);
            this.tabSettings.TabIndex = 29;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.numericElitismProcent);
            this.tabPage1.Controls.Add(this.numericColonySize);
            this.tabPage1.Controls.Add(this.numericAlpha);
            this.tabPage1.Controls.Add(this.lblRHO);
            this.tabPage1.Controls.Add(this.numericGenerationsNumber);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.lblQ);
            this.tabPage1.Controls.Add(this.numericBeta);
            this.tabPage1.Controls.Add(this.numericQ);
            this.tabPage1.Controls.Add(this.numericRHO);
            this.tabPage1.Controls.Add(this.lblBeta);
            this.tabPage1.Controls.Add(this.lblAlpha);
            this.tabPage1.Controls.Add(this.lblPopulationSize);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(566, 211);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "AntColony";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(376, 9);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(52, 17);
            this.label5.TabIndex = 24;
            this.label5.Text = "Elitism:";
            // 
            // numericElitismProcent
            // 
            this.numericElitismProcent.DecimalPlaces = 2;
            this.numericElitismProcent.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericElitismProcent.Location = new System.Drawing.Point(429, 8);
            this.numericElitismProcent.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericElitismProcent.Name = "numericElitismProcent";
            this.numericElitismProcent.Size = new System.Drawing.Size(131, 22);
            this.numericElitismProcent.TabIndex = 23;
            this.numericElitismProcent.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericElitismProcent.Value = new decimal(new int[] {
            20,
            0,
            0,
            131072});
            // 
            // numericColonySize
            // 
            this.numericColonySize.Location = new System.Drawing.Point(132, 7);
            this.numericColonySize.Name = "numericColonySize";
            this.numericColonySize.Size = new System.Drawing.Size(225, 22);
            this.numericColonySize.TabIndex = 22;
            this.numericColonySize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericColonySize.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numericAlpha
            // 
            this.numericAlpha.Location = new System.Drawing.Point(132, 40);
            this.numericAlpha.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericAlpha.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericAlpha.Name = "numericAlpha";
            this.numericAlpha.Size = new System.Drawing.Size(225, 22);
            this.numericAlpha.TabIndex = 21;
            this.numericAlpha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericAlpha.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            // 
            // lblRHO
            // 
            this.lblRHO.AutoSize = true;
            this.lblRHO.Location = new System.Drawing.Point(10, 105);
            this.lblRHO.Name = "lblRHO";
            this.lblRHO.Size = new System.Drawing.Size(43, 17);
            this.lblRHO.TabIndex = 20;
            this.lblRHO.Text = "RHO:";
            // 
            // numericGenerationsNumber
            // 
            this.numericGenerationsNumber.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericGenerationsNumber.Location = new System.Drawing.Point(132, 177);
            this.numericGenerationsNumber.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericGenerationsNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericGenerationsNumber.Name = "numericGenerationsNumber";
            this.numericGenerationsNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericGenerationsNumber.Size = new System.Drawing.Size(225, 22);
            this.numericGenerationsNumber.TabIndex = 18;
            this.numericGenerationsNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericGenerationsNumber.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 177);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Max time(rounds):";
            // 
            // lblQ
            // 
            this.lblQ.AutoSize = true;
            this.lblQ.Location = new System.Drawing.Point(13, 137);
            this.lblQ.Name = "lblQ";
            this.lblQ.Size = new System.Drawing.Size(23, 17);
            this.lblQ.TabIndex = 16;
            this.lblQ.Text = "Q:";
            // 
            // numericBeta
            // 
            this.numericBeta.Location = new System.Drawing.Point(132, 73);
            this.numericBeta.Maximum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericBeta.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericBeta.Name = "numericBeta";
            this.numericBeta.Size = new System.Drawing.Size(225, 22);
            this.numericBeta.TabIndex = 15;
            this.numericBeta.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericBeta.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numericQ
            // 
            this.numericQ.DecimalPlaces = 2;
            this.numericQ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericQ.Location = new System.Drawing.Point(132, 135);
            this.numericQ.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericQ.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericQ.Name = "numericQ";
            this.numericQ.Size = new System.Drawing.Size(225, 22);
            this.numericQ.TabIndex = 12;
            this.numericQ.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericQ.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // numericRHO
            // 
            this.numericRHO.DecimalPlaces = 2;
            this.numericRHO.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericRHO.Location = new System.Drawing.Point(132, 105);
            this.numericRHO.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericRHO.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericRHO.Name = "numericRHO";
            this.numericRHO.Size = new System.Drawing.Size(225, 22);
            this.numericRHO.TabIndex = 11;
            this.numericRHO.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericRHO.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            // 
            // lblBeta
            // 
            this.lblBeta.AutoSize = true;
            this.lblBeta.Location = new System.Drawing.Point(10, 73);
            this.lblBeta.Name = "lblBeta";
            this.lblBeta.Size = new System.Drawing.Size(41, 17);
            this.lblBeta.TabIndex = 6;
            this.lblBeta.Text = "Beta:";
            // 
            // lblAlpha
            // 
            this.lblAlpha.AutoSize = true;
            this.lblAlpha.Location = new System.Drawing.Point(10, 42);
            this.lblAlpha.Name = "lblAlpha";
            this.lblAlpha.Size = new System.Drawing.Size(48, 17);
            this.lblAlpha.TabIndex = 4;
            this.lblAlpha.Text = "Alpha:";
            // 
            // lblPopulationSize
            // 
            this.lblPopulationSize.AutoSize = true;
            this.lblPopulationSize.Location = new System.Drawing.Point(7, 7);
            this.lblPopulationSize.Name = "lblPopulationSize";
            this.lblPopulationSize.Size = new System.Drawing.Size(92, 17);
            this.lblPopulationSize.TabIndex = 0;
            this.lblPopulationSize.Text = "Ants number:";
            // 
            // tabConsole
            // 
            this.tabConsole.Controls.Add(this.richConsole);
            this.tabConsole.Location = new System.Drawing.Point(4, 25);
            this.tabConsole.Name = "tabConsole";
            this.tabConsole.Padding = new System.Windows.Forms.Padding(3);
            this.tabConsole.Size = new System.Drawing.Size(566, 211);
            this.tabConsole.TabIndex = 1;
            this.tabConsole.Text = "Console";
            this.tabConsole.UseVisualStyleBackColor = true;
            // 
            // richConsole
            // 
            this.richConsole.Location = new System.Drawing.Point(8, 7);
            this.richConsole.Name = "richConsole";
            this.richConsole.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richConsole.Size = new System.Drawing.Size(552, 198);
            this.richConsole.TabIndex = 0;
            this.richConsole.Text = "";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(1059, 443);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(89, 31);
            this.btnStop.TabIndex = 28;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(958, 443);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(85, 31);
            this.btnStart.TabIndex = 27;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // numericCities
            // 
            this.numericCities.Location = new System.Drawing.Point(68, 448);
            this.numericCities.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericCities.Name = "numericCities";
            this.numericCities.Size = new System.Drawing.Size(164, 22);
            this.numericCities.TabIndex = 26;
            this.numericCities.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericCities.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 450);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 25;
            this.label1.Text = "Cities:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(238, 443);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(112, 31);
            this.btnGenerate.TabIndex = 24;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // countryMap
            // 
            this.countryMap.Location = new System.Drawing.Point(10, 25);
            this.countryMap.Name = "countryMap";
            this.countryMap.Size = new System.Drawing.Size(542, 403);
            this.countryMap.TabIndex = 23;
            // 
            // tabChart
            // 
            this.tabChart.Controls.Add(this.chartAnts);
            this.tabChart.Location = new System.Drawing.Point(4, 25);
            this.tabChart.Name = "tabChart";
            this.tabChart.Padding = new System.Windows.Forms.Padding(3);
            this.tabChart.Size = new System.Drawing.Size(566, 211);
            this.tabChart.TabIndex = 2;
            this.tabChart.Text = "Chart";
            this.tabChart.UseVisualStyleBackColor = true;
            // 
            // chartAnts
            // 
            chartArea1.Name = "ChartArea1";
            this.chartAnts.ChartAreas.Add(chartArea1);
            this.chartAnts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAnts.Location = new System.Drawing.Point(3, 3);
            this.chartAnts.Name = "chartAnts";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Name = "Series1";
            this.chartAnts.Series.Add(series1);
            this.chartAnts.Size = new System.Drawing.Size(560, 205);
            this.chartAnts.TabIndex = 0;
            this.chartAnts.Text = "Ants chart";
            // 
            // AntsUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupCurrentGeneration);
            this.Controls.Add(this.tabSettings);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.numericCities);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.countryMap);
            this.Name = "AntsUC";
            this.Size = new System.Drawing.Size(1162, 510);
            this.Load += new System.EventHandler(this.AntsUC_Load);
            this.groupCurrentGeneration.ResumeLayout(false);
            this.groupCurrentGeneration.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericElitismProcent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericColonySize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericAlpha)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGenerationsNumber)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericBeta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericQ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericRHO)).EndInit();
            this.tabConsole.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numericCities)).EndInit();
            this.tabChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAnts)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupCurrentGeneration;
        private System.Windows.Forms.Label lblCurrentTimeBestDistance;
        private System.Windows.Forms.Label lblCurrentTimeValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericElitismProcent;
        private System.Windows.Forms.NumericUpDown numericColonySize;
        private System.Windows.Forms.NumericUpDown numericAlpha;
        private System.Windows.Forms.Label lblRHO;
        private System.Windows.Forms.NumericUpDown numericGenerationsNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblQ;
        private System.Windows.Forms.NumericUpDown numericBeta;
        private System.Windows.Forms.NumericUpDown numericQ;
        private System.Windows.Forms.NumericUpDown numericRHO;
        private System.Windows.Forms.Label lblBeta;
        private System.Windows.Forms.Label lblAlpha;
        private System.Windows.Forms.Label lblPopulationSize;
        private System.Windows.Forms.TabPage tabConsole;
        private System.Windows.Forms.RichTextBox richConsole;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.NumericUpDown numericCities;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGenerate;
        private Accord.Controls.Chart countryMap;
        private System.Windows.Forms.TabPage tabChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAnts;
    }
}
