﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.Controls;
using Accord;
using NaturalOptimizationAlgorithmsTSP.SimulatedAnnealingAlgorithm;

namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    public partial class SimulatedAnnealingUC : UserControl
    {
        int numCities = 20;
        private double[,] map = null;

        public SimulatedAnnealingUC()
        {
            InitializeComponent();

            countryMap.RangeX = new Range(0, 1000);
            countryMap.RangeY = new Range(0, 1000);
            countryMap.AddDataSeries("map", Color.Red, Chart.SeriesType.Dots, 5, false);
            countryMap.AddDataSeries("path", Color.Blue, Chart.SeriesType.Line, 1, false);
        }

        public void generateMap()
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            map = new double[numCities, 2];

            for (int i = 0; i < numCities; i++)
            {
                map[i, 0] = rnd.Next(1001);
                map[i, 1] = rnd.Next(1001);
            }

            countryMap.UpdateDataSeries("map", map);
            countryMap.UpdateDataSeries("path", null);
        }

        public void setDefaultValues()
        {
            this.numCities = Convert.ToInt32(this.numericCities.Value);
        }

        public void UpdateSettings()
        {
            this.numCities = Convert.ToInt32(this.numericCities.Value);
        }

        private void upDateConsole(City[] bestRoute)
        {
            string console = "";
            for (int i = 0; i < bestRoute.Length - 1; i++)
                console += "From city [" + bestRoute[i].X + "," + bestRoute[i].Y + "] to [" + bestRoute[i + 1].X + "," + bestRoute[i + 1].Y
                           + "] - distance = " + bestRoute[i].distanceFrom(bestRoute[i + 1]) + Environment.NewLine;

            richConsole.Text = console;
        }

        private void upDateMap(City[] bestRoute)
        {
            double[,] path = new double[numCities + 1, 2];
            for (int i = 0; i < bestRoute.Length; i++)
            {
                path[i, 0] = bestRoute[i].X;
                path[i, 1] = bestRoute[i].Y;
            }
            path[bestRoute.Length, 0] = bestRoute[0].X;
            path[bestRoute.Length, 1] = bestRoute[0].Y;

            this.lblCurrentGenerationBestDistance.Text = getDistance(bestRoute).ToString();

            countryMap.UpdateDataSeries("path", path);
        }

        public double getDistance(City[] route)
        {
            double totalDistance = 0;
            for (int cityIndex = 0; cityIndex + 1 < route.Length; cityIndex++)
                totalDistance += route[cityIndex].distanceFrom(route[cityIndex + 1]);

            totalDistance += route[route.Length - 1].distanceFrom(route[0]);
            return totalDistance;
        }

        void SearchSolution()//nije
        {
            City[] cities = new City[numCities];
            Random rnd = new Random();

            for (int cityIndex = 0; cityIndex < numCities; cityIndex++)
            {
                double xPos = map[cityIndex, 0];
                double yPos = map[cityIndex, 1];
                cities[cityIndex] = new City(xPos, yPos);
            }


            

            //sa.solve();
            //upDateMap(sa.Cities);
            //upDateConsole(sa.Cities);
            //GreedyAlgorithm.GreedyAlgorithm ga = new GreedyAlgorithm.GreedyAlgorithm(cities);
            //upDateMap(ga.Slove());
            //upDateConsole(ga.Cities);
            //NNAlgorithm nna = new NNAlgorithm(cities);
            //upDateMap(nna.Slove());
            //upDateConsole(nna.Cities);
        }

        private void btnGenerate_Click(object sender, EventArgs e)
        {
            setDefaultValues();
            generateMap();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            richConsole.Text = "";
            SearchSolution();
        }

        private void SimulatedAnnealingUC_Load(object sender, EventArgs e)
        {
            setDefaultValues();
            generateMap();
        }
    }
}
