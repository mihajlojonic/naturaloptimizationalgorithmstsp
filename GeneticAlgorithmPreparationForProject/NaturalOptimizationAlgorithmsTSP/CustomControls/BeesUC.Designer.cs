﻿namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    partial class BeesUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.groupCurrentGeneration = new System.Windows.Forms.GroupBox();
            this.lblCurrentTimeBestDistance = new System.Windows.Forms.Label();
            this.lblCurrentTimeValue = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.numericMaxNumberOfRounds = new System.Windows.Forms.NumericUpDown();
            this.numericMaxNumberOfVisits = new System.Windows.Forms.NumericUpDown();
            this.numericProbPersuasion = new System.Windows.Forms.NumericUpDown();
            this.numericProbMistake = new System.Windows.Forms.NumericUpDown();
            this.numericInactiveBees = new System.Windows.Forms.NumericUpDown();
            this.numericScoutBees = new System.Windows.Forms.NumericUpDown();
            this.numericActiveBees = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.numericColonySize = new System.Windows.Forms.NumericUpDown();
            this.tabConsole = new System.Windows.Forms.TabPage();
            this.richConsole = new System.Windows.Forms.RichTextBox();
            this.tabChart = new System.Windows.Forms.TabPage();
            this.chartAnts = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.numericCities = new System.Windows.Forms.NumericUpDown();
            this.label1 = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.countryMap = new Accord.Controls.Chart();
            this.groupInitialize = new System.Windows.Forms.GroupBox();
            this.comboInitialize = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.numericSpecialInitialize = new System.Windows.Forms.NumericUpDown();
            this.groupCurrentGeneration.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxNumberOfRounds)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxNumberOfVisits)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericProbPersuasion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericProbMistake)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericInactiveBees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericScoutBees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericActiveBees)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericColonySize)).BeginInit();
            this.tabConsole.SuspendLayout();
            this.tabChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartAnts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCities)).BeginInit();
            this.groupInitialize.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpecialInitialize)).BeginInit();
            this.SuspendLayout();
            // 
            // groupCurrentGeneration
            // 
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentTimeBestDistance);
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentTimeValue);
            this.groupCurrentGeneration.Controls.Add(this.label4);
            this.groupCurrentGeneration.Controls.Add(this.label3);
            this.groupCurrentGeneration.Location = new System.Drawing.Point(578, 294);
            this.groupCurrentGeneration.Name = "groupCurrentGeneration";
            this.groupCurrentGeneration.Size = new System.Drawing.Size(570, 140);
            this.groupCurrentGeneration.TabIndex = 38;
            this.groupCurrentGeneration.TabStop = false;
            this.groupCurrentGeneration.Text = "Current generation";
            // 
            // lblCurrentTimeBestDistance
            // 
            this.lblCurrentTimeBestDistance.AutoSize = true;
            this.lblCurrentTimeBestDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentTimeBestDistance.ForeColor = System.Drawing.Color.Blue;
            this.lblCurrentTimeBestDistance.Location = new System.Drawing.Point(228, 83);
            this.lblCurrentTimeBestDistance.Name = "lblCurrentTimeBestDistance";
            this.lblCurrentTimeBestDistance.Size = new System.Drawing.Size(84, 39);
            this.lblCurrentTimeBestDistance.TabIndex = 3;
            this.lblCurrentTimeBestDistance.Text = "0.00";
            // 
            // lblCurrentTimeValue
            // 
            this.lblCurrentTimeValue.AutoSize = true;
            this.lblCurrentTimeValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentTimeValue.ForeColor = System.Drawing.Color.Red;
            this.lblCurrentTimeValue.Location = new System.Drawing.Point(221, 29);
            this.lblCurrentTimeValue.Name = "lblCurrentTimeValue";
            this.lblCurrentTimeValue.Size = new System.Drawing.Size(36, 39);
            this.lblCurrentTimeValue.TabIndex = 2;
            this.lblCurrentTimeValue.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.8F);
            this.label4.Location = new System.Drawing.Point(6, 83);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(201, 36);
            this.label4.TabIndex = 1;
            this.label4.Text = "Best distance:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.8F);
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(200, 36);
            this.label3.TabIndex = 0;
            this.label3.Text = "Time(rounds):";
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.tabPage1);
            this.tabSettings.Controls.Add(this.tabConsole);
            this.tabSettings.Controls.Add(this.tabChart);
            this.tabSettings.Location = new System.Drawing.Point(578, 31);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(574, 257);
            this.tabSettings.TabIndex = 37;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupInitialize);
            this.tabPage1.Controls.Add(this.numericMaxNumberOfRounds);
            this.tabPage1.Controls.Add(this.numericMaxNumberOfVisits);
            this.tabPage1.Controls.Add(this.numericProbPersuasion);
            this.tabPage1.Controls.Add(this.numericProbMistake);
            this.tabPage1.Controls.Add(this.numericInactiveBees);
            this.tabPage1.Controls.Add(this.numericScoutBees);
            this.tabPage1.Controls.Add(this.numericActiveBees);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.numericColonySize);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(566, 228);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "BeeColony";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // numericMaxNumberOfRounds
            // 
            this.numericMaxNumberOfRounds.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericMaxNumberOfRounds.Location = new System.Drawing.Point(172, 203);
            this.numericMaxNumberOfRounds.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.numericMaxNumberOfRounds.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericMaxNumberOfRounds.Name = "numericMaxNumberOfRounds";
            this.numericMaxNumberOfRounds.Size = new System.Drawing.Size(99, 22);
            this.numericMaxNumberOfRounds.TabIndex = 38;
            this.numericMaxNumberOfRounds.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericMaxNumberOfRounds.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // numericMaxNumberOfVisits
            // 
            this.numericMaxNumberOfVisits.Location = new System.Drawing.Point(172, 176);
            this.numericMaxNumberOfVisits.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
            this.numericMaxNumberOfVisits.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMaxNumberOfVisits.Name = "numericMaxNumberOfVisits";
            this.numericMaxNumberOfVisits.Size = new System.Drawing.Size(99, 22);
            this.numericMaxNumberOfVisits.TabIndex = 37;
            this.numericMaxNumberOfVisits.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericMaxNumberOfVisits.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numericProbPersuasion
            // 
            this.numericProbPersuasion.DecimalPlaces = 2;
            this.numericProbPersuasion.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericProbPersuasion.Location = new System.Drawing.Point(401, 203);
            this.numericProbPersuasion.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericProbPersuasion.Name = "numericProbPersuasion";
            this.numericProbPersuasion.Size = new System.Drawing.Size(157, 22);
            this.numericProbPersuasion.TabIndex = 36;
            this.numericProbPersuasion.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericProbPersuasion.Value = new decimal(new int[] {
            95,
            0,
            0,
            131072});
            // 
            // numericProbMistake
            // 
            this.numericProbMistake.DecimalPlaces = 2;
            this.numericProbMistake.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericProbMistake.Location = new System.Drawing.Point(401, 179);
            this.numericProbMistake.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericProbMistake.Name = "numericProbMistake";
            this.numericProbMistake.Size = new System.Drawing.Size(157, 22);
            this.numericProbMistake.TabIndex = 35;
            this.numericProbMistake.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericProbMistake.Value = new decimal(new int[] {
            5,
            0,
            0,
            131072});
            // 
            // numericInactiveBees
            // 
            this.numericInactiveBees.DecimalPlaces = 2;
            this.numericInactiveBees.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericInactiveBees.Location = new System.Drawing.Point(157, 96);
            this.numericInactiveBees.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericInactiveBees.Name = "numericInactiveBees";
            this.numericInactiveBees.Size = new System.Drawing.Size(281, 22);
            this.numericInactiveBees.TabIndex = 34;
            this.numericInactiveBees.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericInactiveBees.Value = new decimal(new int[] {
            10,
            0,
            0,
            131072});
            // 
            // numericScoutBees
            // 
            this.numericScoutBees.DecimalPlaces = 2;
            this.numericScoutBees.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericScoutBees.Location = new System.Drawing.Point(157, 66);
            this.numericScoutBees.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericScoutBees.Name = "numericScoutBees";
            this.numericScoutBees.Size = new System.Drawing.Size(281, 22);
            this.numericScoutBees.TabIndex = 33;
            this.numericScoutBees.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericScoutBees.Value = new decimal(new int[] {
            15,
            0,
            0,
            131072});
            // 
            // numericActiveBees
            // 
            this.numericActiveBees.DecimalPlaces = 2;
            this.numericActiveBees.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericActiveBees.Location = new System.Drawing.Point(157, 36);
            this.numericActiveBees.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericActiveBees.Name = "numericActiveBees";
            this.numericActiveBees.Size = new System.Drawing.Size(281, 22);
            this.numericActiveBees.TabIndex = 32;
            this.numericActiveBees.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericActiveBees.Value = new decimal(new int[] {
            75,
            0,
            0,
            131072});
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(303, 205);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(83, 17);
            this.label11.TabIndex = 31;
            this.label11.Text = "Persuasion:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(303, 181);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 17);
            this.label10.TabIndex = 30;
            this.label10.Text = "Mistake:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 205);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(157, 17);
            this.label9.TabIndex = 29;
            this.label9.Text = "Max. number of rounds:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(9, 181);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(144, 17);
            this.label8.TabIndex = 28;
            this.label8.Text = "Max. number of visits:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(103, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(48, 17);
            this.label7.TabIndex = 27;
            this.label7.Text = "Scout:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(101, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(50, 17);
            this.label6.TabIndex = 26;
            this.label6.Text = "Active:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(91, 100);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 17);
            this.label5.TabIndex = 25;
            this.label5.Text = "Inactive:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(55, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 24;
            this.label2.Text = "Bees number:";
            // 
            // numericColonySize
            // 
            this.numericColonySize.Location = new System.Drawing.Point(157, 6);
            this.numericColonySize.Name = "numericColonySize";
            this.numericColonySize.Size = new System.Drawing.Size(281, 22);
            this.numericColonySize.TabIndex = 23;
            this.numericColonySize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericColonySize.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // tabConsole
            // 
            this.tabConsole.Controls.Add(this.richConsole);
            this.tabConsole.Location = new System.Drawing.Point(4, 25);
            this.tabConsole.Name = "tabConsole";
            this.tabConsole.Padding = new System.Windows.Forms.Padding(3);
            this.tabConsole.Size = new System.Drawing.Size(566, 228);
            this.tabConsole.TabIndex = 1;
            this.tabConsole.Text = "Console";
            this.tabConsole.UseVisualStyleBackColor = true;
            // 
            // richConsole
            // 
            this.richConsole.Location = new System.Drawing.Point(8, 7);
            this.richConsole.Name = "richConsole";
            this.richConsole.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richConsole.Size = new System.Drawing.Size(552, 198);
            this.richConsole.TabIndex = 0;
            this.richConsole.Text = "";
            // 
            // tabChart
            // 
            this.tabChart.Controls.Add(this.chartAnts);
            this.tabChart.Location = new System.Drawing.Point(4, 25);
            this.tabChart.Name = "tabChart";
            this.tabChart.Padding = new System.Windows.Forms.Padding(3);
            this.tabChart.Size = new System.Drawing.Size(566, 228);
            this.tabChart.TabIndex = 2;
            this.tabChart.Text = "Chart";
            this.tabChart.UseVisualStyleBackColor = true;
            // 
            // chartAnts
            // 
            chartArea2.Name = "ChartArea1";
            this.chartAnts.ChartAreas.Add(chartArea2);
            this.chartAnts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chartAnts.Location = new System.Drawing.Point(3, 3);
            this.chartAnts.Name = "chartAnts";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Name = "Series1";
            this.chartAnts.Series.Add(series2);
            this.chartAnts.Size = new System.Drawing.Size(560, 222);
            this.chartAnts.TabIndex = 0;
            this.chartAnts.Text = "Ants chart";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(1059, 449);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(89, 31);
            this.btnStop.TabIndex = 36;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(958, 449);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(85, 31);
            this.btnStart.TabIndex = 35;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // numericCities
            // 
            this.numericCities.Location = new System.Drawing.Point(68, 454);
            this.numericCities.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numericCities.Name = "numericCities";
            this.numericCities.Size = new System.Drawing.Size(164, 22);
            this.numericCities.TabIndex = 34;
            this.numericCities.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericCities.Value = new decimal(new int[] {
            25,
            0,
            0,
            0});
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 456);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 33;
            this.label1.Text = "Cities:";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(238, 449);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(112, 31);
            this.btnGenerate.TabIndex = 32;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // countryMap
            // 
            this.countryMap.Location = new System.Drawing.Point(10, 31);
            this.countryMap.Name = "countryMap";
            this.countryMap.Size = new System.Drawing.Size(542, 403);
            this.countryMap.TabIndex = 31;
            // 
            // groupInitialize
            // 
            this.groupInitialize.Controls.Add(this.numericSpecialInitialize);
            this.groupInitialize.Controls.Add(this.label12);
            this.groupInitialize.Controls.Add(this.comboInitialize);
            this.groupInitialize.Location = new System.Drawing.Point(12, 115);
            this.groupInitialize.Name = "groupInitialize";
            this.groupInitialize.Size = new System.Drawing.Size(546, 55);
            this.groupInitialize.TabIndex = 39;
            this.groupInitialize.TabStop = false;
            this.groupInitialize.Text = "Initialize";
            // 
            // comboInitialize
            // 
            this.comboInitialize.FormattingEnabled = true;
            this.comboInitialize.Items.AddRange(new object[] {
            "Random",
            "Nearest Neighbour"});
            this.comboInitialize.Location = new System.Drawing.Point(7, 22);
            this.comboInitialize.Name = "comboInitialize";
            this.comboInitialize.Size = new System.Drawing.Size(289, 24);
            this.comboInitialize.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(302, 25);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(112, 17);
            this.label12.TabIndex = 1;
            this.label12.Text = "Procent of bees:";
            // 
            // numericSpecialInitialize
            // 
            this.numericSpecialInitialize.DecimalPlaces = 2;
            this.numericSpecialInitialize.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericSpecialInitialize.Location = new System.Drawing.Point(420, 23);
            this.numericSpecialInitialize.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericSpecialInitialize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.numericSpecialInitialize.Name = "numericSpecialInitialize";
            this.numericSpecialInitialize.Size = new System.Drawing.Size(120, 22);
            this.numericSpecialInitialize.TabIndex = 2;
            this.numericSpecialInitialize.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            // 
            // BeesUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupCurrentGeneration);
            this.Controls.Add(this.tabSettings);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.numericCities);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.countryMap);
            this.Name = "BeesUC";
            this.Size = new System.Drawing.Size(1162, 510);
            this.Load += new System.EventHandler(this.BeesUC_Load);
            this.groupCurrentGeneration.ResumeLayout(false);
            this.groupCurrentGeneration.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxNumberOfRounds)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxNumberOfVisits)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericProbPersuasion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericProbMistake)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericInactiveBees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericScoutBees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericActiveBees)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericColonySize)).EndInit();
            this.tabConsole.ResumeLayout(false);
            this.tabChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartAnts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericCities)).EndInit();
            this.groupInitialize.ResumeLayout(false);
            this.groupInitialize.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericSpecialInitialize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupCurrentGeneration;
        private System.Windows.Forms.Label lblCurrentTimeBestDistance;
        private System.Windows.Forms.Label lblCurrentTimeValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabConsole;
        private System.Windows.Forms.RichTextBox richConsole;
        private System.Windows.Forms.TabPage tabChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartAnts;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.NumericUpDown numericCities;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnGenerate;
        private Accord.Controls.Chart countryMap;
        private System.Windows.Forms.NumericUpDown numericColonySize;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numericMaxNumberOfRounds;
        private System.Windows.Forms.NumericUpDown numericMaxNumberOfVisits;
        private System.Windows.Forms.NumericUpDown numericProbPersuasion;
        private System.Windows.Forms.NumericUpDown numericProbMistake;
        private System.Windows.Forms.NumericUpDown numericInactiveBees;
        private System.Windows.Forms.NumericUpDown numericScoutBees;
        private System.Windows.Forms.NumericUpDown numericActiveBees;
        private System.Windows.Forms.GroupBox groupInitialize;
        private System.Windows.Forms.NumericUpDown numericSpecialInitialize;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboInitialize;
    }
}
