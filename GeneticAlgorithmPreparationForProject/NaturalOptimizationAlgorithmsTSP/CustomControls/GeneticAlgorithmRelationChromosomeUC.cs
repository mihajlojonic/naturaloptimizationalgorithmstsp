﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using Accord;
using System.Windows.Forms.DataVisualization.Charting;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithmReletionChromosome;

namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    public partial class GeneticAlgorithmRelationChromosomeUC : UserControl
    {
        String consoleOutput = "";

        private volatile bool needToStop = false;

        int elitismCount = 1;
        double mutationRate = 0.25;
        double crossoverRate = 0.75;
        int turnamentSize = 2;
        int populationSize = 100;
        double nnaRate = 0.1;
        double greedyRate = 0.1;

        int numCities = 20;
        private double[,] map = null;

        private Thread workerThread = null;

        public int ElitismCount { get => elitismCount; set => elitismCount = value; }
        public double MutationRate { get => mutationRate; set => mutationRate = value; }
        public double CrossoverRate { get => crossoverRate; set => crossoverRate = value; }
        public int TurnamentSize { get => turnamentSize; set => turnamentSize = value; }
        public int PopulationSize { get => populationSize; set => populationSize = value; }
        public double NnaRate { get => nnaRate; set => nnaRate = value; }
        public double GreedyRate { get => greedyRate; set => greedyRate = value; }

        public GeneticAlgorithmRelationChromosomeUC()
        {
            InitializeComponent();

            countryMap.RangeX = new Range(0, 1000);
            countryMap.RangeY = new Range(0, 1000); 
            countryMap.AddDataSeries("map", Color.Red, Accord.Controls.Chart.SeriesType.Dots, 5, false);
            countryMap.AddDataSeries("path", Color.Blue, Accord.Controls.Chart.SeriesType.Line, 1, false);
        }

        private void GeneticAlgorithmRelationChromosomeUC_Load(object sender, EventArgs e)
        {
            setDefaultValues();
            generateMap();
        }

        public void generateMap()
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            map = new double[numCities, 2];

            for (int i = 0; i < numCities; i++)
            {
                map[i, 0] = rnd.Next(1001);
                map[i, 1] = rnd.Next(1001);
            }

            countryMap.UpdateDataSeries("map", map);
            countryMap.UpdateDataSeries("path", null);
        }

        public void setDefaultValues()
        {
            this.numericElitismRate.Value = 0.20m;
            this.numericGenerationsNumber.Value = 10m;

            this.numCities = Convert.ToInt32(this.numericCitiesCount.Value);

            this.PopulationSize = Int32.Parse(this.numericPopulationSize.Text);

            this.ElitismCount = Convert.ToInt32(this.numericElitismRate.Value);
        }

        public void UpdateSettings()
        {
            chartGenetic.Series[0].Points.Clear();

            this.numCities = Convert.ToInt32(this.numericCitiesCount.Value);

            this.PopulationSize = Int32.Parse(this.numericPopulationSize.Text);
            this.ElitismCount = Convert.ToInt32(this.populationSize * Convert.ToDouble(this.numericElitismRate.Value));
        }

        void SearchSolution()
        {
            City[] cities = new City[numCities];
            Random rnd = new Random();

            for (int cityIndex = 0; cityIndex < numCities; cityIndex++)
            {
                double xPos = map[cityIndex, 0];
                double yPos = map[cityIndex, 1];
                cities[cityIndex] = new City(xPos, yPos);
            }

            GeneticAlgorithmRelation ga = new GeneticAlgorithmRelation(PopulationSize, MutationRate, CrossoverRate, ElitismCount, TurnamentSize);

            Population population = ga.initPopulation(cities.Length, 0, cities);
            ga.evalPopulation(population);

            int generation = 1;

            while (!ga.isTerminateConditionMet(generation, Convert.ToInt32(this.numericGenerationsNumber.Value)) && !needToStop)
            {
                //Route route = new Route(population.getFittest(0), cities);
                Individual fittest = population.getFittest(0);

                //prikaz
                consoleOutput = consoleOutput + Environment.NewLine + "G" + generation + " Best distance: " + fittest.getRouteDistance();
                //richConsole.Text = consoleOutput;
                //upDateChart(generation, fittest.getRouteDistance());
                upDateMap(fittest, generation);


                population = ga.crossoverPopulation(population);

                population = ga.mutatePopulation(population);

                ga.evalPopulation(population);

                generation++;
            }
        }

        private void upDateChart(int generation, double v)
        {
            chartGenetic.ChartAreas[0].CursorX.IsUserEnabled = true;
            chartGenetic.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            //chartGenetic.ChartAreas[0].AxisX.ScaleView.Zoomable = true;

            chartGenetic.Series[0].Points.AddXY(generation, v);

        }

        private void upDateMap(Individual fittest, int generation)
        {
            double[,] path = new double[numCities + 1, 2];
            
            for (int i = 0; i < fittest.Chromosome.Length; i++)
            {
                path[i, 0] = fittest.Chromosome[i].City1.X;
                path[i, 1] = fittest.Chromosome[i].City1.Y;
            }


            //this.lblCurrentGenerationValue.Text = generation.ToString();
            //this.lblCurrentGenerationBestDistance.Text = fittest.getRouteDistance().ToString();

            countryMap.UpdateDataSeries("path", path);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            UpdateSettings();
            needToStop = false;
            consoleOutput = "";
            workerThread = new Thread(new ThreadStart(SearchSolution));
            workerThread.Start();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (workerThread != null)
            {
                needToStop = true;
                while (!workerThread.Join(100))
                    Application.DoEvents();
                workerThread = null;
            }
        }

        private void btnGenerateCitiesCount_Click(object sender, EventArgs e)
        {
            this.numCities = Convert.ToInt32(this.numericCitiesCount.Value);
            generateMap();
        }
    }
}
