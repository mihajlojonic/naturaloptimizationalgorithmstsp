﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Selections;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Mutations;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm.Corossovers;
using Accord;
using Accord.Controls;
using NaturalOptimizationAlgorithmsTSP.GeneticAlgorithm;
using NaturalOptimizationAlgorithmsTSP.NearestNeighbourAlgorithm;
using NaturalOptimizationAlgorithmsTSP.MassPointAlgorithm;

namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    public partial class GeneticUC : UserControl
    {
        String consoleOutput = "";

        private volatile bool needToStop = false;

        int elitismCount = 1;
        double mutationRate = 0.25;
        double crossoverRate = 0.75;
        int turnamentSize = 2;
        int populationSize = 100;
        double nnaRate = 0.1;
        double greedyRate = 0.1;

        IMutation chosenMutation;
        ICrossover chosenCrossover;
        ISelection chosenSelection;
        int numCities = 20;
        private double[,] map = null;

        private Thread workerThread = null;

        public int ElitismCount { get => elitismCount; set => elitismCount = value; }
        public double MutationRate { get => mutationRate; set => mutationRate = value; }
        public double CrossoverRate { get => crossoverRate; set => crossoverRate = value; }
        public int TurnamentSize { get => turnamentSize; set => turnamentSize = value; }
        public int PopulationSize { get => populationSize; set => populationSize = value; }
        public double NnaRate { get => nnaRate; set => nnaRate = value; }
        public double GreedyRate { get => greedyRate; set => greedyRate = value; }

        public GeneticUC()
        {
            InitializeComponent();

            countryMap.RangeX = new Range(0, 1000);
            countryMap.RangeY = new Range(0, 1000);
            countryMap.AddDataSeries("map", Color.Red, Chart.SeriesType.Dots, 5, false);
            countryMap.AddDataSeries("path", Color.Blue, Chart.SeriesType.Line, 1, false);
        }

        private void GeneticUC_Load(object sender, EventArgs e)
        {
            setDefaultValues();
            generateMap();
        }

        public void generateMap()
        {
            Random rnd = new Random((int)DateTime.Now.Ticks);
            map = new double[numCities, 2];

            for (int i = 0; i < numCities; i++)
            {
                map[i, 0] = rnd.Next(1001);
                map[i, 1] = rnd.Next(1001);
            }

            countryMap.UpdateDataSeries("map", map);
            countryMap.UpdateDataSeries("path", null);
        }

        public void setDefaultValues()
        {
            this.comboSelection.SelectedIndex = 1;
            this.comboMutation.SelectedIndex = 0;
            this.comboCrossover.SelectedIndex = 0;
            this.numericElitismRate.Value = 0.20m;
            this.numericMutationRate.Value = 0.05m;
            this.numericTurnamentSize.Value = 0.80m;
            this.numericCrossoverRate.Value = 0.95m;
            this.numericGenerationsNumber.Value = 10m;

            this.numCities = Convert.ToInt32(this.numericCitiesCount.Value);

            this.PopulationSize = Int32.Parse(this.numericPopulationSize.Text);
            this.TurnamentSize = Convert.ToInt32(this.PopulationSize * Convert.ToDouble(this.numericTurnamentSize.Value));
            this.CrossoverRate = Convert.ToDouble(this.numericElitismRate.Value);
            this.MutationRate = Convert.ToDouble(this.numericMutationRate.Value);
            this.ElitismCount = Convert.ToInt32(this.numericElitismRate.Value);

            this.NnaRate = Convert.ToDouble(this.numericNNA.Value);
            this.GreedyRate = Convert.ToDouble(this.numericGreedy.Value);

            chosenMutation = new SwapMutation(ElitismCount, MutationRate);
            chosenSelection = new TurnamentSelection(TurnamentSize);
            chosenCrossover = new OrderedCrossover(ElitismCount, CrossoverRate, chosenSelection);
        }

        public void UpdateSettings()
        {
            chartGenetic.Series[0].Points.Clear();

            this.numCities = Convert.ToInt32(this.numericCitiesCount.Value);

            this.PopulationSize = Int32.Parse(this.numericPopulationSize.Text);
            this.TurnamentSize = Convert.ToInt32(this.PopulationSize * Convert.ToDouble(this.numericTurnamentSize.Value));
            this.CrossoverRate = Convert.ToDouble(this.numericCrossoverRate.Value);
            this.MutationRate = Convert.ToDouble(this.numericMutationRate.Value);
            this.ElitismCount = Convert.ToInt32(this.populationSize * Convert.ToDouble(this.numericElitismRate.Value));

            this.NnaRate = Convert.ToDouble(this.numericNNA.Value);
            this.GreedyRate = Convert.ToDouble(this.numericGreedy.Value);

            chosenMutation = GetMutation();
            chosenSelection = getSelection();
            chosenCrossover = getCrossover();
        }

        private IMutation GetMutation()
        {
            string mutation = this.comboSelection.SelectedText;
            switch (mutation)
            {
                case "Swap mutation":
                    return new SwapMutation(ElitismCount, MutationRate);
                case "Scramble mutation":
                    return new ScrambleMutation(ElitismCount, MutationRate);
                case "Inversion mutation":
                    return new InversionMutation(ElitismCount, MutationRate);
                case "Uniform mutation":
                    return new UniformMutation(ElitismCount, MutationRate);
                default:
                    return new SwapMutation(ElitismCount, MutationRate);
            }
        }

        private ISelection getSelection()
        {
            string selection = this.comboSelection.SelectedText;
            switch (selection)
            {
                case "Roulete wheel slection":
                    return new RouletteWheelSelection();
                case "Rank selection":
                    return new RankSelection();
                case "Elite selection":
                    return new EliteSelection();
                case "Turnament selection":
                    return new TurnamentSelection(TurnamentSize);
                default:
                    return new TurnamentSelection(TurnamentSize);
            }
        }

        private ICrossover getCrossover()
        {
            string crossover = this.comboCrossover.SelectedText;
            switch (crossover)
            {
                case "Ordered crossover":
                    return new OrderedCrossover(ElitismCount, CrossoverRate, getSelection());
                case "SinglePoint crossover":
                    return new SinglePointCrossover(ElitismCount, CrossoverRate, getSelection());
                case "Random crossover":
                    return new RandomCrossover(ElitismCount, CrossoverRate, getSelection());
                default:
                    return new OrderedCrossover(ElitismCount, CrossoverRate, getSelection());
            }
        }
        
        void SearchSolution()
        {
            City[] cities = new City[numCities];
            Random rnd = new Random();
            
            int countOfSpecialInitialize = 0;

            for (int cityIndex = 0; cityIndex < numCities; cityIndex++)
            {
                double xPos = map[cityIndex, 0];
                double yPos = map[cityIndex, 1];
                cities[cityIndex] = new City(xPos, yPos);
            }

            if (radioNearestNeighbour.Checked)
            {
                NNAlgorithm nna = new NNAlgorithm(cities);
                nna.Slove();
                cities = nna.Cities;
                countOfSpecialInitialize = Convert.ToInt32(numCities * this.numericNNA.Value);
            }

            if(radioGreedy.Checked)
            {
                GreedyAlgorithm.GreedyAlgorithm greedyAlg = new GreedyAlgorithm.GreedyAlgorithm(cities);
                greedyAlg.Slove();
                cities = greedyAlg.Cities;
                countOfSpecialInitialize = Convert.ToInt32(numCities * this.numericGreedy.Value);
            }

            if(radioMassPoint.Checked)
            {
                MassPAlgorithm mpa = new MassPAlgorithm(cities);
                mpa.Slove();
                cities = mpa.Cities;
                countOfSpecialInitialize = Convert.ToInt32(numCities * this.numericMassPoint.Value);
            }

            
            GeneticAlgorithm.GeneticAlgorithm ga = new GeneticAlgorithm.GeneticAlgorithm(PopulationSize, MutationRate, CrossoverRate, ElitismCount, TurnamentSize, chosenMutation, chosenCrossover,chosenSelection);
            
            Population population = ga.initPopulation(cities.Length, countOfSpecialInitialize);
            ga.evalPopulation(population, cities);

            int generation = 1;

            while (!ga.isTerminateConditionMet(generation, Convert.ToInt32(this.numericGenerationsNumber.Value)) && !needToStop)
            {
                Route route = new Route(population.getFittest(0), cities);

                //prikaz
                consoleOutput = consoleOutput + Environment.NewLine + "G" + generation + " Best distance: " + route.getDistance();
                richConsole.Text = consoleOutput;
                upDateChart(generation, route.getDistance());
                upDateMap(route, generation);

                
                population = ga.crossoverPopulation(population);

                population = ga.mutatePopulation(population);

                ga.evalPopulation(population, cities);

                generation++;
            }
        }

        private void upDateChart(int generation, double v)
        {
             chartGenetic.ChartAreas[0].CursorX.IsUserEnabled = true;
            chartGenetic.ChartAreas[0].CursorX.IsUserSelectionEnabled = true;
            //chartGenetic.ChartAreas[0].AxisX.ScaleView.Zoomable = true;

            chartGenetic.Series[0].Points.AddXY(generation, v);
            
        }

        private void upDateMap(Route route, int generation)
        {
            double[,] path = new double[numCities + 1, 2];
            City[] bestRoute = route.route;
            for (int i = 0; i < bestRoute.Length; i++)
            {
                path[i, 0] = bestRoute[i].X;
                path[i, 1] = bestRoute[i].Y;
            }
            path[bestRoute.Length, 0] = bestRoute[0].X;
            path[bestRoute.Length, 1] = bestRoute[0].Y;

            this.lblCurrentGenerationValue.Text = generation.ToString();
            this.lblCurrentGenerationBestDistance.Text = route.getDistance().ToString();

            countryMap.UpdateDataSeries("path", path);
        }

        

        private void btnGenerateCitiesCount_Click(object sender, EventArgs e)
        {
            this.numCities = Convert.ToInt32(this.numericCitiesCount.Value);
            generateMap();
        }

        private void btnStop_Click(object sender, EventArgs e)
        {
            if (workerThread != null)
            {
                needToStop = true;
                while (!workerThread.Join(100))
                    Application.DoEvents();
                workerThread = null;
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            UpdateSettings();
            needToStop = false;
            consoleOutput = "";
            workerThread = new Thread(new ThreadStart(SearchSolution));
            workerThread.Start();
        }

        private void comboSelection_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.comboSelection.Text == "Turnament selection")
            {
                lblTurnamentSize.Enabled = true;
                numericTurnamentSize.Enabled = true;
            }
            else
            {
                lblTurnamentSize.Enabled = false;
                numericTurnamentSize.Enabled = false;
            }
        }

    }
}
