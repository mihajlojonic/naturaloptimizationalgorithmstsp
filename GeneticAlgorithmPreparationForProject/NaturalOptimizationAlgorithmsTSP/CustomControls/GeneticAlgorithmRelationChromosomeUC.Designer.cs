﻿namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    partial class GeneticAlgorithmRelationChromosomeUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.gbMapCountry = new System.Windows.Forms.GroupBox();
            this.numericCitiesCount = new System.Windows.Forms.NumericUpDown();
            this.btnGenerateCitiesCount = new System.Windows.Forms.Button();
            this.countryMap = new Accord.Controls.Chart();
            this.label1 = new System.Windows.Forms.Label();
            this.btnStop = new System.Windows.Forms.Button();
            this.groupCurrentGeneration = new System.Windows.Forms.GroupBox();
            this.lblCurrentGenerationBestDistance = new System.Windows.Forms.Label();
            this.lblCurrentGenerationValue = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.numericPopulationSize = new System.Windows.Forms.NumericUpDown();
            this.numericElitismRate = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numericGenerationsNumber = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.lblPopulationSize = new System.Windows.Forms.Label();
            this.tabConsole = new System.Windows.Forms.TabPage();
            this.richConsole = new System.Windows.Forms.RichTextBox();
            this.tabLiveChart = new System.Windows.Forms.TabPage();
            this.chartGenetic = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.label5 = new System.Windows.Forms.Label();
            this.gbMapCountry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCitiesCount)).BeginInit();
            this.groupCurrentGeneration.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericPopulationSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericElitismRate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGenerationsNumber)).BeginInit();
            this.tabConsole.SuspendLayout();
            this.tabLiveChart.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartGenetic)).BeginInit();
            this.SuspendLayout();
            // 
            // gbMapCountry
            // 
            this.gbMapCountry.Controls.Add(this.numericCitiesCount);
            this.gbMapCountry.Controls.Add(this.btnGenerateCitiesCount);
            this.gbMapCountry.Controls.Add(this.countryMap);
            this.gbMapCountry.Controls.Add(this.label1);
            this.gbMapCountry.Location = new System.Drawing.Point(14, 13);
            this.gbMapCountry.Name = "gbMapCountry";
            this.gbMapCountry.Size = new System.Drawing.Size(555, 485);
            this.gbMapCountry.TabIndex = 30;
            this.gbMapCountry.TabStop = false;
            this.gbMapCountry.Text = "Country map";
            // 
            // numericCitiesCount
            // 
            this.numericCitiesCount.Location = new System.Drawing.Point(71, 451);
            this.numericCitiesCount.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericCitiesCount.Name = "numericCitiesCount";
            this.numericCitiesCount.Size = new System.Drawing.Size(120, 22);
            this.numericCitiesCount.TabIndex = 1;
            this.numericCitiesCount.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // btnGenerateCitiesCount
            // 
            this.btnGenerateCitiesCount.Location = new System.Drawing.Point(225, 443);
            this.btnGenerateCitiesCount.Name = "btnGenerateCitiesCount";
            this.btnGenerateCitiesCount.Size = new System.Drawing.Size(105, 36);
            this.btnGenerateCitiesCount.TabIndex = 3;
            this.btnGenerateCitiesCount.Text = "Generate";
            this.btnGenerateCitiesCount.UseVisualStyleBackColor = true;
            this.btnGenerateCitiesCount.Click += new System.EventHandler(this.btnGenerateCitiesCount_Click);
            // 
            // countryMap
            // 
            this.countryMap.Location = new System.Drawing.Point(7, 17);
            this.countryMap.Name = "countryMap";
            this.countryMap.Size = new System.Drawing.Size(542, 420);
            this.countryMap.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 453);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Cities:";
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(1021, 456);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(124, 36);
            this.btnStop.TabIndex = 28;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // groupCurrentGeneration
            // 
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentGenerationBestDistance);
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentGenerationValue);
            this.groupCurrentGeneration.Controls.Add(this.label4);
            this.groupCurrentGeneration.Controls.Add(this.label3);
            this.groupCurrentGeneration.Location = new System.Drawing.Point(575, 384);
            this.groupCurrentGeneration.Name = "groupCurrentGeneration";
            this.groupCurrentGeneration.Size = new System.Drawing.Size(570, 66);
            this.groupCurrentGeneration.TabIndex = 29;
            this.groupCurrentGeneration.TabStop = false;
            this.groupCurrentGeneration.Text = "Current generation";
            // 
            // lblCurrentGenerationBestDistance
            // 
            this.lblCurrentGenerationBestDistance.AutoSize = true;
            this.lblCurrentGenerationBestDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentGenerationBestDistance.ForeColor = System.Drawing.Color.Blue;
            this.lblCurrentGenerationBestDistance.Location = new System.Drawing.Point(381, 18);
            this.lblCurrentGenerationBestDistance.Name = "lblCurrentGenerationBestDistance";
            this.lblCurrentGenerationBestDistance.Size = new System.Drawing.Size(84, 39);
            this.lblCurrentGenerationBestDistance.TabIndex = 3;
            this.lblCurrentGenerationBestDistance.Text = "0.00";
            // 
            // lblCurrentGenerationValue
            // 
            this.lblCurrentGenerationValue.AutoSize = true;
            this.lblCurrentGenerationValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentGenerationValue.ForeColor = System.Drawing.Color.Red;
            this.lblCurrentGenerationValue.Location = new System.Drawing.Point(138, 18);
            this.lblCurrentGenerationValue.Name = "lblCurrentGenerationValue";
            this.lblCurrentGenerationValue.Size = new System.Drawing.Size(36, 39);
            this.lblCurrentGenerationValue.TabIndex = 2;
            this.lblCurrentGenerationValue.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F);
            this.label4.Location = new System.Drawing.Point(226, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(149, 26);
            this.label4.TabIndex = 1;
            this.label4.Text = "Best distance:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.8F);
            this.label3.Location = new System.Drawing.Point(7, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 26);
            this.label3.TabIndex = 0;
            this.label3.Text = "Generation:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(856, 456);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(131, 36);
            this.btnStart.TabIndex = 27;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.tabPage1);
            this.tabSettings.Controls.Add(this.tabConsole);
            this.tabSettings.Controls.Add(this.tabLiveChart);
            this.tabSettings.Location = new System.Drawing.Point(575, 13);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(574, 365);
            this.tabSettings.TabIndex = 26;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.numericPopulationSize);
            this.tabPage1.Controls.Add(this.numericElitismRate);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.numericGenerationsNumber);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.lblPopulationSize);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(566, 336);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "TravelingSalesman";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // numericPopulationSize
            // 
            this.numericPopulationSize.Location = new System.Drawing.Point(132, 7);
            this.numericPopulationSize.Name = "numericPopulationSize";
            this.numericPopulationSize.Size = new System.Drawing.Size(225, 22);
            this.numericPopulationSize.TabIndex = 22;
            this.numericPopulationSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericPopulationSize.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // numericElitismRate
            // 
            this.numericElitismRate.DecimalPlaces = 2;
            this.numericElitismRate.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.numericElitismRate.Location = new System.Drawing.Point(482, 11);
            this.numericElitismRate.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericElitismRate.Name = "numericElitismRate";
            this.numericElitismRate.Size = new System.Drawing.Size(57, 22);
            this.numericElitismRate.TabIndex = 21;
            this.numericElitismRate.Value = new decimal(new int[] {
            20,
            0,
            0,
            131072});
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(367, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 17);
            this.label7.TabIndex = 20;
            this.label7.Text = "Elitism rate:";
            // 
            // numericGenerationsNumber
            // 
            this.numericGenerationsNumber.Increment = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericGenerationsNumber.Location = new System.Drawing.Point(132, 311);
            this.numericGenerationsNumber.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericGenerationsNumber.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericGenerationsNumber.Name = "numericGenerationsNumber";
            this.numericGenerationsNumber.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.numericGenerationsNumber.Size = new System.Drawing.Size(225, 22);
            this.numericGenerationsNumber.TabIndex = 18;
            this.numericGenerationsNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericGenerationsNumber.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 313);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(121, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Generations num:";
            // 
            // lblPopulationSize
            // 
            this.lblPopulationSize.AutoSize = true;
            this.lblPopulationSize.Location = new System.Drawing.Point(10, 7);
            this.lblPopulationSize.Name = "lblPopulationSize";
            this.lblPopulationSize.Size = new System.Drawing.Size(108, 17);
            this.lblPopulationSize.TabIndex = 0;
            this.lblPopulationSize.Text = "Population size:";
            // 
            // tabConsole
            // 
            this.tabConsole.Controls.Add(this.richConsole);
            this.tabConsole.Location = new System.Drawing.Point(4, 25);
            this.tabConsole.Name = "tabConsole";
            this.tabConsole.Padding = new System.Windows.Forms.Padding(3);
            this.tabConsole.Size = new System.Drawing.Size(566, 336);
            this.tabConsole.TabIndex = 1;
            this.tabConsole.Text = "Console";
            this.tabConsole.UseVisualStyleBackColor = true;
            // 
            // richConsole
            // 
            this.richConsole.Location = new System.Drawing.Point(8, 7);
            this.richConsole.Name = "richConsole";
            this.richConsole.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richConsole.Size = new System.Drawing.Size(552, 302);
            this.richConsole.TabIndex = 0;
            this.richConsole.Text = "";
            // 
            // tabLiveChart
            // 
            this.tabLiveChart.Controls.Add(this.chartGenetic);
            this.tabLiveChart.Location = new System.Drawing.Point(4, 25);
            this.tabLiveChart.Name = "tabLiveChart";
            this.tabLiveChart.Padding = new System.Windows.Forms.Padding(3);
            this.tabLiveChart.Size = new System.Drawing.Size(566, 336);
            this.tabLiveChart.TabIndex = 2;
            this.tabLiveChart.Text = "Chart";
            this.tabLiveChart.UseVisualStyleBackColor = true;
            // 
            // chartGenetic
            // 
            chartArea5.Name = "ChartArea1";
            this.chartGenetic.ChartAreas.Add(chartArea5);
            this.chartGenetic.Location = new System.Drawing.Point(8, 16);
            this.chartGenetic.Name = "chartGenetic";
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Spline;
            series5.Name = "Series1";
            this.chartGenetic.Series.Add(series5);
            this.chartGenetic.Size = new System.Drawing.Size(552, 300);
            this.chartGenetic.TabIndex = 0;
            this.chartGenetic.Text = "Chart - Genetic algorithm";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.label5.ForeColor = System.Drawing.Color.Red;
            this.label5.Location = new System.Drawing.Point(13, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(526, 225);
            this.label5.TabIndex = 23;
            this.label5.Text = "January exam - 3. exercise - Relational Chromosome Genetic Algorithm";
            // 
            // GeneticAlgorithmRelationChromosomeUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gbMapCountry);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.groupCurrentGeneration);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.tabSettings);
            this.Name = "GeneticAlgorithmRelationChromosomeUC";
            this.Size = new System.Drawing.Size(1163, 510);
            this.Load += new System.EventHandler(this.GeneticAlgorithmRelationChromosomeUC_Load);
            this.gbMapCountry.ResumeLayout(false);
            this.gbMapCountry.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCitiesCount)).EndInit();
            this.groupCurrentGeneration.ResumeLayout(false);
            this.groupCurrentGeneration.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericPopulationSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericElitismRate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericGenerationsNumber)).EndInit();
            this.tabConsole.ResumeLayout(false);
            this.tabLiveChart.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartGenetic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbMapCountry;
        private System.Windows.Forms.NumericUpDown numericCitiesCount;
        private System.Windows.Forms.Button btnGenerateCitiesCount;
        private Accord.Controls.Chart countryMap;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.GroupBox groupCurrentGeneration;
        private System.Windows.Forms.Label lblCurrentGenerationBestDistance;
        private System.Windows.Forms.Label lblCurrentGenerationValue;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericPopulationSize;
        private System.Windows.Forms.NumericUpDown numericElitismRate;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericGenerationsNumber;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblPopulationSize;
        private System.Windows.Forms.TabPage tabConsole;
        private System.Windows.Forms.RichTextBox richConsole;
        private System.Windows.Forms.TabPage tabLiveChart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartGenetic;
    }
}
