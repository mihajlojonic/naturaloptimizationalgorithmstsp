﻿namespace NaturalOptimizationAlgorithmsTSP.CustomControls
{
    partial class NearestNeighbourUC
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbMapCountry = new System.Windows.Forms.GroupBox();
            this.numericCities = new System.Windows.Forms.NumericUpDown();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.countryMap = new Accord.Controls.Chart();
            this.label2 = new System.Windows.Forms.Label();
            this.groupCurrentGeneration = new System.Windows.Forms.GroupBox();
            this.lblCurrentGenerationBestDistance = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.tabSettings = new System.Windows.Forms.TabControl();
            this.tabConsole = new System.Windows.Forms.TabPage();
            this.richConsole = new System.Windows.Forms.RichTextBox();
            this.gbMapCountry.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCities)).BeginInit();
            this.groupCurrentGeneration.SuspendLayout();
            this.tabSettings.SuspendLayout();
            this.tabConsole.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMapCountry
            // 
            this.gbMapCountry.Controls.Add(this.numericCities);
            this.gbMapCountry.Controls.Add(this.btnGenerate);
            this.gbMapCountry.Controls.Add(this.countryMap);
            this.gbMapCountry.Controls.Add(this.label2);
            this.gbMapCountry.Location = new System.Drawing.Point(12, 12);
            this.gbMapCountry.Name = "gbMapCountry";
            this.gbMapCountry.Size = new System.Drawing.Size(555, 485);
            this.gbMapCountry.TabIndex = 26;
            this.gbMapCountry.TabStop = false;
            this.gbMapCountry.Text = "Country map";
            // 
            // numericCities
            // 
            this.numericCities.Location = new System.Drawing.Point(71, 451);
            this.numericCities.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericCities.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericCities.Name = "numericCities";
            this.numericCities.Size = new System.Drawing.Size(120, 22);
            this.numericCities.TabIndex = 1;
            this.numericCities.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericCities.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(225, 443);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(105, 36);
            this.btnGenerate.TabIndex = 3;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.UseVisualStyleBackColor = true;
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // countryMap
            // 
            this.countryMap.Location = new System.Drawing.Point(7, 21);
            this.countryMap.Name = "countryMap";
            this.countryMap.Size = new System.Drawing.Size(542, 403);
            this.countryMap.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 453);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Cities:";
            // 
            // groupCurrentGeneration
            // 
            this.groupCurrentGeneration.Controls.Add(this.lblCurrentGenerationBestDistance);
            this.groupCurrentGeneration.Controls.Add(this.label4);
            this.groupCurrentGeneration.Location = new System.Drawing.Point(573, 276);
            this.groupCurrentGeneration.Name = "groupCurrentGeneration";
            this.groupCurrentGeneration.Size = new System.Drawing.Size(570, 160);
            this.groupCurrentGeneration.TabIndex = 27;
            this.groupCurrentGeneration.TabStop = false;
            this.groupCurrentGeneration.Text = "Distance";
            // 
            // lblCurrentGenerationBestDistance
            // 
            this.lblCurrentGenerationBestDistance.AutoSize = true;
            this.lblCurrentGenerationBestDistance.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.8F);
            this.lblCurrentGenerationBestDistance.ForeColor = System.Drawing.Color.Blue;
            this.lblCurrentGenerationBestDistance.Location = new System.Drawing.Point(222, 53);
            this.lblCurrentGenerationBestDistance.Name = "lblCurrentGenerationBestDistance";
            this.lblCurrentGenerationBestDistance.Size = new System.Drawing.Size(84, 39);
            this.lblCurrentGenerationBestDistance.TabIndex = 3;
            this.lblCurrentGenerationBestDistance.Text = "0.00";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 17.8F);
            this.label4.Location = new System.Drawing.Point(6, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(201, 36);
            this.label4.TabIndex = 1;
            this.label4.Text = "Best distance:";
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(1012, 455);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(131, 36);
            this.btnStart.TabIndex = 28;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.tabConsole);
            this.tabSettings.Location = new System.Drawing.Point(573, 21);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.SelectedIndex = 0;
            this.tabSettings.Size = new System.Drawing.Size(574, 240);
            this.tabSettings.TabIndex = 30;
            // 
            // tabConsole
            // 
            this.tabConsole.Controls.Add(this.richConsole);
            this.tabConsole.Location = new System.Drawing.Point(4, 25);
            this.tabConsole.Name = "tabConsole";
            this.tabConsole.Padding = new System.Windows.Forms.Padding(3);
            this.tabConsole.Size = new System.Drawing.Size(566, 211);
            this.tabConsole.TabIndex = 1;
            this.tabConsole.Text = "Console";
            this.tabConsole.UseVisualStyleBackColor = true;
            // 
            // richConsole
            // 
            this.richConsole.Location = new System.Drawing.Point(8, 7);
            this.richConsole.Name = "richConsole";
            this.richConsole.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this.richConsole.Size = new System.Drawing.Size(552, 198);
            this.richConsole.TabIndex = 0;
            this.richConsole.Text = "";
            // 
            // NearestNeighbourUC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tabSettings);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.groupCurrentGeneration);
            this.Controls.Add(this.gbMapCountry);
            this.Name = "NearestNeighbourUC";
            this.Size = new System.Drawing.Size(1162, 510);
            this.Load += new System.EventHandler(this.NearestNeighbourUC_Load);
            this.gbMapCountry.ResumeLayout(false);
            this.gbMapCountry.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericCities)).EndInit();
            this.groupCurrentGeneration.ResumeLayout(false);
            this.groupCurrentGeneration.PerformLayout();
            this.tabSettings.ResumeLayout(false);
            this.tabConsole.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbMapCountry;
        private System.Windows.Forms.NumericUpDown numericCities;
        private System.Windows.Forms.Button btnGenerate;
        private Accord.Controls.Chart countryMap;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupCurrentGeneration;
        private System.Windows.Forms.Label lblCurrentGenerationBestDistance;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.TabControl tabSettings;
        private System.Windows.Forms.TabPage tabConsole;
        private System.Windows.Forms.RichTextBox richConsole;
    }
}
