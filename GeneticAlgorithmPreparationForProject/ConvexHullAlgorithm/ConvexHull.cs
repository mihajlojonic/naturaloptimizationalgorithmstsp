﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvexHullAlgorithm
{
    public class ConvexHull
    {
        public static List<Point> MakeHull(List<Point> points)
        {
            List<Point> newPoints = new List<Point>(points);
            newPoints.Sort();
            return MakeHullPresorted(newPoints);
        }

        private static List<Point> MakeHullPresorted(List<Point> points)
        {
            if (points.Count <= 1)
                return new List<Point>(points);

            List<Point> upperHull = new List<Point>();
            foreach(Point p in points)
            {
                while(upperHull.Count >= 2)
                {
                    Point q = upperHull[upperHull.Count - 1];
                    Point r = upperHull[upperHull.Count - 2];
                    if ((q.x - r.x) * (p.y - r.y) >= (q.y - r.y) * (p.x - r.x))
                        upperHull.RemoveAt(upperHull.Count - 1);
                    else
                        break;
                }
                upperHull.Add(p);
            }
            upperHull.RemoveAt(upperHull.Count - 1);

            List<Point> lowerHull = new List<Point>();
            for(int i = points.Count - 1; i >= 0; i--)
            {
                Point p = points[i];
                while(lowerHull.Count >= 2)
                {
                    Point q = lowerHull[lowerHull.Count - 1];
                    Point r = lowerHull[lowerHull.Count - 2];
                    if ((q.x - r.x) * (p.y - r.y) >= (q.y - r.y) * (p.x - r.x))
                        lowerHull.RemoveAt(lowerHull.Count - 1);
                    else
                        break;
                }
                lowerHull.Add(p);
            }
            lowerHull.RemoveAt(lowerHull.Count - 1);

            if (!(upperHull.Count == 1 && Enumerable.SequenceEqual(upperHull, lowerHull)))
                upperHull.AddRange(lowerHull);
            return upperHull;
        }
    }
}
