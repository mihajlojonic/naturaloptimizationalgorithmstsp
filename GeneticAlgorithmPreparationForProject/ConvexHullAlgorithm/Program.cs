﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConvexHullAlgorithm
{
    class Program
    {
        static void Main(string[] args)
        {
            TestEmpty();
            TestOne();
            TestTwoDuplicate();
            TestTwoHorizontal0();
            TestTwoHorizontal1();
            TestTwoVertical0();
            TestTwoVertical1();
            TestTwoDiagonal0();
            TestTwoDiagonal1();
            TestRectangle();
        }

        private static void TestEmpty()
        {
            List<Point> points = new List<Point>();
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = new List<Point>();
            AssertEquals(expect, actual);
        }

        private static void TestOne()
        {
            List<Point> points = new List<Point> { new Point(3, 1) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = points;
            AssertEquals(expect, actual);
        }


        private static void TestTwoDuplicate()
        {
            List<Point> points = new List<Point> { new Point(0, 0), new Point(0, 0) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = new List<Point> { new Point(0, 0) };
            AssertEquals(expect, actual);
        }


        private static void TestTwoHorizontal0()
        {
            List<Point> points = new List<Point> { new Point(2, 0), new Point(5, 0) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = points;
            AssertEquals(expect, actual);
        }


        private static void TestTwoHorizontal1()
        {
            List<Point> points = new List<Point> { new Point(-6, -3), new Point(-8, -3) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = new List<Point> { new Point(-8, -3), new Point(-6, -3) };
            AssertEquals(expect, actual);
        }


        private static void TestTwoVertical0()
        {
            List<Point> points = new List<Point> { new Point(1, -4), new Point(1, 4) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = points;
            AssertEquals(expect, actual);
        }


        private static void TestTwoVertical1()
        {
            List<Point> points = new List<Point> { new Point(-1, 2), new Point(-1, -3) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = new List<Point> { new Point(-1, -3), new Point(-1, 2) };
            AssertEquals(expect, actual);
        }


        private static void TestTwoDiagonal0()
        {
            List<Point> points = new List<Point> { new Point(-2, -3), new Point(2, 0) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = points;
            AssertEquals(expect, actual);
        }


        private static void TestTwoDiagonal1()
        {
            List<Point> points = new List<Point> { new Point(-2, 3), new Point(2, 0) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = points;
            AssertEquals(expect, actual);
        }


        private static void TestRectangle()
        {
            List<Point> points = new List<Point> { new Point(-3, 2), new Point(1, 2), new Point(1, -4), new Point(-3, -4) };
            List<Point> actual = ConvexHull.MakeHull(points);
            List<Point> expect = new List<Point> { new Point(-3, -4), new Point(-3, 2), new Point(1, 2), new Point(1, -4) };
            AssertEquals(expect, actual);
        }

        private static void AssertEquals(IList<Point> a, IList<Point> b)
        {
            if (!Enumerable.SequenceEqual(a, b))
                Console.WriteLine("Not Valid!");
            else
                Console.WriteLine("Valid!");
        }
    }
}
