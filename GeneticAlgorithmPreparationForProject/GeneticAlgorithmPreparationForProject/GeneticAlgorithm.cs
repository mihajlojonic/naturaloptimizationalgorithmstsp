﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmPreparationForProject
{
    class GeneticAlgorithm
    {
        private int populationSize;
        private double mutationRate;
        private double crossoverRate;
        private int elitismCount;

        public GeneticAlgorithm(int populationSize, double mutationRate, double crossoverRate, int elitismCount)
        {
            this.populationSize = populationSize;
            this.mutationRate = mutationRate;
            this.crossoverRate = crossoverRate;
            this.elitismCount = elitismCount;
        }

        public Population initPopulation(int chromosomeLength)
        {
            Population population = new Population(this.populationSize, chromosomeLength);
            return population;
        }

        public double calcFitness(Individual individual)
        {
            int correctGenes = 0;
            for(int geneIndex = 0; geneIndex < individual.getChromosomeLength(); geneIndex++)
            {
                if (individual.getGene(geneIndex) == 1)
                    correctGenes++;
            }
            double fitness = (double)correctGenes / individual.getChromosomeLength();
            individual.setFitness(fitness);
            return fitness;
        }

        public void evalPopulation(Population population)
        {
            double populationFitness = 0;
            foreach(Individual individual in population.getIndividuals())
                populationFitness += calcFitness(individual);
     
            population.setPopulationFitness(populationFitness);
        }

        public bool isTerminateConditionMet(Population population)
        {
            foreach(Individual individual in population.getIndividuals())
                if (individual.getFitness() == 1)
                    return true;
            return false;
        }

        public Individual selectParent(Population population)
        {
            Individual[] individuals = population.getIndividuals();

            Random rnd = new Random();
            double populationFitness = population.getPopulationFitness();
            double rouletteWheelPosition = rnd.NextDouble() * populationFitness;

            double spinWheel = 0;
            foreach(Individual individual in individuals)
            {
                spinWheel += individual.getFitness();
                if (spinWheel >= rouletteWheelPosition)
                    return individual;
            }
            return individuals[population.size() - 1];
        }

        public Population crossoverPopulation(Population population)
        {
            Population newPopulation = new Population(population.size());
            Random rnd = new Random();
            for(int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual parent1 = population.getFittnest(populationIndex);

                if (this.crossoverRate > rnd.NextDouble() && populationIndex > this.elitismCount)
                {
                    Individual offspring = new Individual(parent1.getChromosomeLength());

                    Individual parent2 = selectParent(population);

                    for (int geneIndex = 0; geneIndex < parent1.getChromosomeLength(); geneIndex++)
                        if (0.5 > rnd.NextDouble())
                            offspring.setGene(geneIndex, parent1.getGene(geneIndex));
                        else
                            offspring.setGene(geneIndex, parent2.getGene(geneIndex));
                    newPopulation.setIndividual(populationIndex, offspring);
                }
                else
                    newPopulation.setIndividual(populationIndex, parent1);
            }
            return newPopulation;
        }

        public Population mutatePopulation(Population population)
        {
            Population newPopulation = new Population(this.populationSize);
            for(int populationIndex = 0; populationIndex < population.size(); populationIndex++)
            {
                Individual individual = population.getFittnest(populationIndex);
                Random rnd = new Random();
                for(int geneIndex = 0; geneIndex < individual.getChromosomeLength(); geneIndex++)
                {
                    if(populationIndex >= this.elitismCount)
                    {
                        if (this.mutationRate > rnd.NextDouble())
                        {
                            int newGene = 1;
                            if (individual.getGene(geneIndex) == 1)
                                newGene = 0;

                            individual.setGene(geneIndex, newGene);
                        }
                    }
                }
                newPopulation.setIndividual(populationIndex, individual);
            }
            return newPopulation;
        }
    }
}
