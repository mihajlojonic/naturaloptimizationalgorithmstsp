﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmPreparationForProject
{
    class Program
    {
        static void Main(string[] args)
        {
            GeneticAlgorithm ga = new GeneticAlgorithm(100, 0.01, 0.95, 5);
            Population population = ga.initPopulation(50);
            ga.evalPopulation(population);
            int generation = 1;

            while(ga.isTerminateConditionMet(population) == false)
            {
                Console.WriteLine("Best solution: " + population.getFittnest(0).toString());

                population = ga.crossoverPopulation(population);

                population = ga.mutatePopulation(population);

                ga.evalPopulation(population);

                generation++;
            }

            Console.WriteLine("Found solution in " + generation + " generations");
            Console.WriteLine("Best solution: " + population.getFittnest(0).toString());
        }
    }
}