﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneticAlgorithmPreparationForProject
{
    class Population
    {
        private Individual[] population;
        private double populationFitness = -1;

        public Population(int populationSize)
        {
            this.population = new Individual[populationSize];
        }

        public Population(int populationSize, int chromosomeLength)
        {
            this.population = new Individual[populationSize];
            for(int individualCount = 0; individualCount < populationSize; individualCount++)
            {
                Individual individual = new Individual(chromosomeLength);
                this.population[individualCount] = individual;
            }
        }

        public Individual[] getIndividuals()
        {
            return this.population;
        }

        public Individual getFittnest(int offset)
        {
            sortPopulationByFitness();
            return this.population[offset];
        }

        public void sortPopulationByFitness()
        {
            int i, j, max_idx;

            for (i = 0; i < this.population.Length; i++)
            {
                max_idx = i;
                for (j = i + 1; j < this.population.Length; j++)
                    if (population[j].getFitness() > population[max_idx].getFitness())
                        max_idx = j;

                Individual temp = this.population[max_idx];
                this.population[max_idx] = this.population[i];
                this.population[i] = temp;
            }
        }

        public void setPopulationFitness(double fitness)
        {
            this.populationFitness = fitness;
        }

        public double getPopulationFitness()
        {
            return this.populationFitness;
        }

        public int size()
        {
            return this.population.Length;
        }

        public Individual setIndividual(int offset, Individual individual)
        {
            return population[offset] = individual;
        }

        public Individual getIndividual(int offset)
        {
            return population[offset];
        }

        public void shuffle()
        {
            Random rnd = new Random();
            for(int i = population.Length - 1; i > 0; i--)
            {
                int index = rnd.Next(i + 1);
                Individual a = population[index];
                population[index] = population[i];
                population[i] = a;
            }
        }
    }
}
